const path = require('path');
const webpackDevServerWaitPage = require('webpack-dev-server-waitpage');

const pkg = require('./package');
const config = require('./build.config');

const waitPageOptions = {
    title: pkg.name,
    theme: 'material',
};

module.exports = {
    contentBase: path.join(__dirname, config.distDirectory),
    clientLogLevel: 'info',
    open: false,
    publicPath: '/',
    hot: true,
    host: '0.0.0.0',
    overlay: true,
    before: (app, server) => {
        app.use(webpackDevServerWaitPage(server, waitPageOptions));
    },
    port: 5555,
    historyApiFallback: true,
};
