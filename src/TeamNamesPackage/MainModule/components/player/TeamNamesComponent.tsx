import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import * as css from './TeamNamesComponent.pcss';
import { Question, Team } from '../../../../CorePackage/MainModule/interfaces';
import { RoundRoute } from '../../../../RoundPackage/MainModule/routes';

export type Props = RouteComponentProps & {
    currentQuestionId?: Question['id'];
    teams: Team[];
};

export const TeamNamesComponent: React.FunctionComponent<Props> = ({
    currentQuestionId,
    history,
    teams,
}) => {
    React.useEffect(() => {
        if (currentQuestionId !== undefined) {
            history.push(RoundRoute.url({ id: currentQuestionId }));
        }
    }, [currentQuestionId]);

    {
        return teams.length === 0 ? (
            <p className={css.text}>Здесь появится список команд</p>
        ) : (
            <ol>
                {teams.map(({ id, name, points }) => (
                    <li key={id} className={css.listItem}>
                        Команда {name} &ndash; {points} очков.
                    </li>
                ))}
            </ol>
        );
    }
};
