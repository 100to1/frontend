import * as React from 'react';
import { RouteComponentProps } from 'react-router';

import * as css from './TeamNamesComponent.pcss';
import { Team } from '../../../../CorePackage/MainModule/interfaces';
import { QuestionsListRoute } from '../../../../QuestionsListPackage/MainModule/routes';
import { classList } from '../../../../CorePackage/MainModule/common';
import { TextField } from '../../../../CorePackage/MainModule/components/TextField';
import { Button } from '../../../../CorePackage/MainModule/components/Button';

export type Props = RouteComponentProps & {
    teams: Team[];
    onSubmit: (value: Team['name']) => void;
};

export const TeamNamesComponent: React.FunctionComponent<Props> = ({
    history,
    teams,
    onSubmit,
}) => {
    const [value, setValue] = React.useState('');

    React.useEffect(() => {
        setValue('');

        if (teams.length >= 2) {
            history.push(QuestionsListRoute.url());
        }
    }, [teams]);

    function handleValueChange(event: React.ChangeEvent<HTMLInputElement>) {
        event.preventDefault();

        setValue(event.currentTarget.value);
    }

    function handleSubmit(event: React.MouseEvent<HTMLButtonElement>) {
        event.preventDefault();

        onSubmit(value);
    }

    return (
        <div
            className={classList({
                container: true,
                [css.component]: true,
            })}
        >
            <form className={css.form}>
                <TextField
                    className={css.textField}
                    value={value}
                    onChange={handleValueChange}
                    label={`Введите название ${teams.length + 1}-й команды`}
                    required={true}
                />
                <Button onClick={handleSubmit}>Подтвердить</Button>
            </form>
        </div>
    );
};
