import { connect, MapStateToProps } from 'react-redux';

import {
    Props as PlayerTeamNamesComponentProps,
    TeamNamesComponent,
} from '../../components/player/TeamNamesComponent';
import { RootState } from '../../../../CorePackage/MainModule/interfaces';
import { getTeams } from '../../../../CorePackage/MainModule/selectors';
import { getRoundCurrentQuestion } from '../../../../RoundPackage/MainModule/selectors';

type Props = PlayerTeamNamesComponentProps;
type StateProps = Pick<Props, 'currentQuestionId' | 'teams'>;
type OwnProps = Pick<Props, never>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    currentQuestionId: getRoundCurrentQuestion(state)?.id,
    teams: getTeams(state),
});

export const TeamNamesContainer = connect(mapStateToProps)(TeamNamesComponent);
