import { Action } from 'redux';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import {
    TeamNamesComponent,
    Props as HostTeamNamesComponentProps,
} from '../../components/host/TeamNamesComponent';
import { RootState } from '../../../../CorePackage/MainModule/interfaces';
import { getTeams } from '../../../../CorePackage/MainModule/selectors';
import { setTeam } from '../../thunks';

type Props = HostTeamNamesComponentProps;
type StateProps = Pick<Props, 'teams'>;
type DispatchProps = Pick<Props, 'onSubmit'>;
type OwnProps = {};

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    teams: getTeams(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch: ThunkDispatch<RootState, void, Action>,
) => ({
    onSubmit: teamName => {
        dispatch(setTeam(teamName));
    },
});

export const TeamNamesContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TeamNamesComponent);
