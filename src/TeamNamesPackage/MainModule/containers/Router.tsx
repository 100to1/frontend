import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { TeamNamesRoute } from '../routes';
import { UserRole } from '../../../CorePackage/MainModule/enums';
import { TeamNamesContainer as HostTeamNamesPage } from './host/TeamNamesContainer';
import { TeamNamesContainer as PlayerTeamNamesPage } from './player/TeamNamesContainer';

type Props = RouteProps & {
    userRole: UserRole;
};

export class Router extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: TeamNamesRoute.url(),
    };

    public render() {
        const { userRole } = this.props;

        return (
            <Route
                {...this.props}
                component={
                    userRole === UserRole.Host
                        ? HostTeamNamesPage
                        : PlayerTeamNamesPage
                }
            />
        );
    }
}
