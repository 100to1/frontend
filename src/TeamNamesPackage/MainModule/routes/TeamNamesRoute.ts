import { AbstractRoute, route } from '../../../CorePackage/RouteModule';

@route()
export class TeamNamesRoute extends AbstractRoute {
    public static url() {
        return '/team-names';
    }
}
