import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import nanoid from 'nanoid';

import { RootState, Team } from '../../../CorePackage/MainModule/interfaces';
import { SetTeamAction } from '../../../CorePackage/MainModule/actions/SetTeamAction';
import { getTeams } from '../../../CorePackage/MainModule/selectors';
import { UserRole } from '../../../CorePackage/MainModule/enums';
import { WebSocketSendMessageAction } from '../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';

export function setTeam(
    teamName: Team['name'],
): ThunkAction<void, RootState, void, Action> {
    return (dispatch, getState) => {
        const state = getState();
        const teams = getTeams(state);

        if (teams.length >= 2) {
            return;
        }

        const newTeam: Team = {
            id: nanoid(),
            name: teamName,
            points: 0,
            errors: 0,
        };

        dispatch(new SetTeamAction(newTeam));
        dispatch(
            new WebSocketSendMessageAction({
                addressee: UserRole.Player,
                action: {
                    type: SetTeamAction.type,
                    payload: newTeam,
                },
            }),
        );
    };
}
