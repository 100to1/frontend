import { RoundName } from '../enums';

export function getRoundSymbolByRoundName(roundName: RoundName): string {
    switch (roundName) {
        case RoundName.Simple:
            return '1';

        case RoundName.Double:
            return '2';

        case RoundName.Triple:
            return '3';

        case RoundName.ViceVersa:
            return '?';
    }
}
