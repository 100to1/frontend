export function getRandomDistributionOfVotes(): number[] {
    function getRandomInRange(min: number, max: number) {
        return Math.random() * (max - min) + min;
    }

    function getNewArrayElement(acc: number[]): number {
        const rand = getRandomInRange(0.35, 0.5);
        const accSum = acc.reduce(
            (accumulator, current) => accumulator + current,
            0,
        );
        const newElement = Math.floor((100 - accSum) * rand);

        if (acc.some(element => newElement >= element)) {
            return getNewArrayElement(acc);
        }

        return newElement;
    }

    return new Array(6).fill('').reduce(acc => {
        const newElement = getNewArrayElement(acc);

        return [...acc, newElement];
    }, []);
}
