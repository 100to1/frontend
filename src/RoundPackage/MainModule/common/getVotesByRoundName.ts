import { RoundName } from '../enums';
import { getRandomDistributionOfVotes } from './getRandomDistributionOfVotes';

export function getVotesByRoundName(roundName: RoundName): number[] {
    switch (roundName) {
        case RoundName.Simple:
            return getRandomDistributionOfVotes();

        case RoundName.Double:
            return getRandomDistributionOfVotes().map(el => el * 2);

        case RoundName.Triple:
            return getRandomDistributionOfVotes().map(el => el * 3);

        case RoundName.ViceVersa:
            return [15, 30, 60, 120, 180, 240];

        default:
            return [];
    }
}
