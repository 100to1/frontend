import { State } from '../interfaces';
import { RoundName } from '../enums';

export const defaultState: State = {
    answers: [],
    bank: 0,
    isFetching: false,
    isStarted: false,
    name: RoundName.Simple,
};
