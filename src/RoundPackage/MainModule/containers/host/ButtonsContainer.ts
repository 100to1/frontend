import { Action } from 'redux';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import {
    ButtonsComponent,
    Props as ButtonsComponentProps,
} from '../../components/host/ButtonsComponent';
import { RootState } from '../../../../CorePackage/MainModule/interfaces';
import {
    getRoundBank,
    getRoundCurrentTeamId,
    getRoundIsStarted,
} from '../../selectors';
import { UserRole } from '../../../../CorePackage/MainModule/enums';
import { changeCurrentTeam, changeTeamPoints, switchRound } from '../../thunks';
import { getTeams } from '../../../../CorePackage/MainModule/selectors';
import { SubmitTeamErrorAction } from '../../../../CorePackage/MainModule/actions/SubmitTeamErrorAction';
import { WebSocketSendMessageAction } from '../../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';

type Props = ButtonsComponentProps;
type StateProps = Pick<Props, 'bank' | 'currentTeamId' | 'isStarted' | 'teams'>;
type DispatchProps = Pick<
    Props,
    | 'onCurrentTeamChange'
    | 'onRoundSwitch'
    | 'onTeamErrorSubmit'
    | 'onTeamPointsChange'
>;
type OwnProps = Pick<Props, never>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    bank: getRoundBank(state),
    currentTeamId: getRoundCurrentTeamId(state),
    isStarted: getRoundIsStarted(state),
    teams: getTeams(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch: ThunkDispatch<RootState, void, Action>,
) => ({
    onCurrentTeamChange: () => {
        dispatch(changeCurrentTeam());
    },
    onRoundSwitch: isStarted => {
        dispatch(switchRound(isStarted));
    },
    onTeamErrorSubmit: (currentTeamId, errors) => {
        const teamData = {
            teamId: currentTeamId,
            errors: errors,
        };

        dispatch(new SubmitTeamErrorAction(teamData));
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SubmitTeamErrorAction.type,
                    payload: teamData,
                },
                addressee: UserRole.Player,
            }),
        );
    },
    onTeamPointsChange: () => {
        dispatch(changeTeamPoints());
    },
});

export const ButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ButtonsComponent);
