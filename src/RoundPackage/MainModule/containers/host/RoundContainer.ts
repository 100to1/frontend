import { Action } from 'redux';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import {
    Props as RoundComponentProps,
    RoundComponent,
} from '../../components/host/RoundComponent';
import { RoundRoute } from '../../routes';
import { RootState } from '../../../../CorePackage/MainModule/interfaces';
import {
    getRoundAnswers,
    getRoundBank,
    getRoundCurrentQuestion,
    getRoundCurrentTeamId,
    getRoundName,
} from '../../selectors';
import { FetchQuestionByIdAction } from '../../actions/FetchQuestionByIdAction';
import { FetchAnswersByQuestionIdAction } from '../../actions/FetchAnswersByQuestionIdAction';
import { SetAnswerIsGuessedAction } from '../../actions/SetAnswerIsGuessedAction';
import { UserRole } from '../../../../CorePackage/MainModule/enums';
import { getTeams } from '../../../../CorePackage/MainModule/selectors';
import { WebSocketSendMessageAction } from '../../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';

type Props = RoundComponentProps & RoundRoute['props'];
type StateProps = Pick<
    Props,
    | 'answers'
    | 'bank'
    | 'currentQuestion'
    | 'currentTeamId'
    | 'roundName'
    | 'teams'
>;
type DispatchProps = Pick<Props, 'onAnswerGuess' | 'onMount'>;
type OwnProps = Pick<Props, 'match'>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    answers: getRoundAnswers(state),
    bank: getRoundBank(state),
    currentQuestion: getRoundCurrentQuestion(state),
    currentTeamId: getRoundCurrentTeamId(state),
    roundName: getRoundName(state),
    teams: getTeams(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch: ThunkDispatch<RootState, void, Action>,
    ownProps,
) => ({
    onAnswerGuess: id => {
        dispatch(new SetAnswerIsGuessedAction(id));
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetAnswerIsGuessedAction.type,
                    payload: id,
                },
                addressee: UserRole.Player,
            }),
        );
    },
    onMount: () => {
        dispatch(new FetchQuestionByIdAction(ownProps.match.params.id));
        dispatch(new FetchAnswersByQuestionIdAction(ownProps.match.params.id));
    },
});

export const RoundContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(RoundComponent);
