import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { RoundRoute } from '../routes';
import { UserRole } from '../../../CorePackage/MainModule/enums';
import { RoundContainer as HostRoundPage } from './host/RoundContainer';
import { RoundContainer as PlayerRoundPage } from './player/RoundContainer';

type Props = RouteProps & {
    userRole: UserRole;
};

export class Router extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: RoundRoute.url(),
    };

    public render() {
        const { userRole } = this.props;
        return (
            <Route
                {...this.props}
                component={
                    userRole === UserRole.Host ? HostRoundPage : PlayerRoundPage
                }
            />
        );
    }
}
