import { Action } from 'redux';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import {
    Props as RoundComponentProps,
    RoundComponent,
} from '../../components/player/RoundComponent';
import { RoundRoute } from '../../routes';
import { RootState } from '../../../../CorePackage/MainModule/interfaces';
import {
    getRoundAnswers,
    getRoundBank,
    getRoundCurrentQuestion,
    getRoundCurrentTeamId,
    getRoundIsStarted,
    getRoundName,
} from '../../selectors';
import { FetchQuestionByIdAction } from '../../actions/FetchQuestionByIdAction';
import { UserRole } from '../../../../CorePackage/MainModule/enums';
import {
    getIsSoundSwitchedOn,
    getTeams,
} from '../../../../CorePackage/MainModule/selectors';
import { fetchAnswers } from '../../thunks';
import { WebSocketSendMessageAction } from '../../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';
import { SetBankPointsAction } from '../../actions/SetBankPointsAction';
import { SetCurrentTeamIdAction } from '../../actions/SetCurrentTeamIdAction';

type Props = RoundComponentProps & RoundRoute['props'];
type StateProps = Pick<
    Props,
    | 'answers'
    | 'bank'
    | 'currentQuestion'
    | 'currentTeamId'
    | 'isSoundSwitchedOn'
    | 'isStarted'
    | 'roundName'
    | 'teams'
>;
type DispatchProps = Pick<
    Props,
    'onBankChange' | 'onCurrentTeamIdSet' | 'onMount'
>;
type OwnProps = Pick<Props, 'match'>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    answers: getRoundAnswers(state),
    bank: getRoundBank(state),
    currentQuestion: getRoundCurrentQuestion(state),
    currentTeamId: getRoundCurrentTeamId(state),
    isSoundSwitchedOn: getIsSoundSwitchedOn(state),
    isStarted: getRoundIsStarted(state),
    roundName: getRoundName(state),
    teams: getTeams(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch: ThunkDispatch<RootState, void, Action>,
    ownProps,
) => ({
    onBankChange: bank => {
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetBankPointsAction.type,
                    payload: bank,
                },
                addressee: UserRole.Host,
            }),
        );
    },
    onCurrentTeamIdSet: teamId => {
        dispatch(new SetCurrentTeamIdAction(teamId));
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetCurrentTeamIdAction.type,
                    payload: teamId,
                },
                addressee: UserRole.Host,
            }),
        );
    },
    onMount: () => {
        dispatch(new FetchQuestionByIdAction(ownProps.match.params.id));
        dispatch(fetchAnswers(ownProps.match.params.id));
    },
});

export const RoundContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(RoundComponent);
