export enum RoundName {
    Simple = 'Простая игра',
    Double = 'Двойная игра',
    Triple = 'Тройная игра',
    ViceVersa = 'Игра наоборот',
}
