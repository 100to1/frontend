import { createSelector } from 'reselect';
import { getRound } from './getRound';

export const getRoundAnswers = createSelector(
    getRound,
    round => round.answers,
);
