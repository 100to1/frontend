import { createSelector } from 'reselect';
import { getRound } from './getRound';

export const getRoundBank = createSelector(getRound, round => round.bank);
