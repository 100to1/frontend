import { createSelector } from 'reselect';
import { getRound } from './getRound';

export const getRoundCurrentQuestion = createSelector(
    getRound,
    round => round.currentQuestion,
);
