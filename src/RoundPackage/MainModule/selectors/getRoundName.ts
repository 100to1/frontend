import { createSelector } from 'reselect';
import { getRound } from './getRound';

export const getRoundName = createSelector(getRound, round => round.name);
