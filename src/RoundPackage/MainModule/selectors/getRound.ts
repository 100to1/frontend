import { createSelector } from 'reselect';
import { RootState } from '../../../CorePackage/MainModule/interfaces';

export const getRound = createSelector(
    (state: RootState) => state,
    state => state.Round,
);
