import { createSelector } from 'reselect';
import { getRound } from './getRound';

export const getRoundIsFetching = createSelector(
    getRound,
    round => round.isFetching,
);
