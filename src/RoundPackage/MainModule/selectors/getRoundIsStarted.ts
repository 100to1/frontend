import { createSelector } from 'reselect';
import { getRound } from './getRound';

export const getRoundIsStarted = createSelector(
    getRound,
    round => round.isStarted,
);
