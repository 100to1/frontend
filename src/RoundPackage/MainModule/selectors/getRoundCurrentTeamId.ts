import { createSelector } from 'reselect';
import { getRound } from './getRound';

export const getRoundCurrentTeamId = createSelector(
    getRound,
    round => round.currentTeamId,
);
