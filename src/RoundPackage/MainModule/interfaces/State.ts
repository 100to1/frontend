import {
    Answer,
    Question,
    Team,
} from '../../../CorePackage/MainModule/interfaces';
import { RoundName } from '../enums';

export interface State {
    answers: Answer[];
    bank: number;
    currentQuestion?: Question;
    currentTeamId?: Team['id'];
    isFetching: boolean;
    isStarted: boolean;
    name: RoundName;
}
