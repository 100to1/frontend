import * as React from 'react';

import * as css from './TeamSelectionComponent.pcss';
import { Team } from '../../../../CorePackage/MainModule/interfaces';
import { classList } from '../../../../CorePackage/MainModule/common';
import { stopAndReplayAudio } from '../../../../CorePackage/MediaModule/common';
import { ShiftKey } from '../../../../CorePackage/MediaModule/components/ShiftKey';

export type Props = {
    isSoundSwitchedOn: boolean;
    teams: Team[];
    onCurrentTeamIdSet: (teamId: Team['id']) => void;
};
const buttonSound = new Audio('/assets/button.mp3');

export const TeamSelectionComponent: React.FunctionComponent<Props> = ({
    isSoundSwitchedOn,
    teams,
    onCurrentTeamIdSet,
}) => {
    function handleKeyDown(event: KeyboardEvent) {
        event.preventDefault();

        if (event.code === 'ShiftLeft') {
            if (isSoundSwitchedOn) {
                stopAndReplayAudio(buttonSound);
            }

            onCurrentTeamIdSet(teams[0].id);
        } else if (event.code === 'ShiftRight') {
            if (isSoundSwitchedOn) {
                stopAndReplayAudio(buttonSound);
            }

            onCurrentTeamIdSet(teams[1].id);
        }
    }

    React.useEffect(() => {
        document.addEventListener('keydown', handleKeyDown);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    });

    return (
        <div
            className={classList({
                container: true,
                [css.component]: true,
            })}
        >
            <header className={css.header}>
                <h2 className={css.title}>Выбор команды</h2>
            </header>
            <div className={css.choice}>
                {teams.map(({ id, name }, index) => (
                    <div key={id} className={css.choiceTeam}>
                        <span className={css.choiceSubtitle}>
                            Команда {name},
                            <br />
                            нажмите {index === 0 ? 'левый' : 'правый'} Shift
                        </span>
                        <div className={css.choiceIcon}>
                            <ShiftKey />
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};
