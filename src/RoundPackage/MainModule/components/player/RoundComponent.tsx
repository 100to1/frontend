import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import * as css from './RoundComponent.pcss';
import {
    Answer,
    Question,
    Team,
} from '../../../../CorePackage/MainModule/interfaces';
import {
    classList,
    makeFirstUpperCase,
} from '../../../../CorePackage/MainModule/common';
import { RoundName } from '../../enums';
import { TeamNamesRoute } from '../../../../TeamNamesPackage/MainModule/routes';
import { stopAndReplayAudio } from '../../../../CorePackage/MediaModule/common';
import { getRoundSymbolByRoundName } from '../../common';
import { TeamSelectionComponent } from './TeamSelectionComponent';

export type Props = RouteComponentProps & {
    answers: Answer[];
    bank: number;
    currentQuestion?: Question;
    currentTeamId?: Team['id'];
    isSoundSwitchedOn: boolean;
    isStarted: boolean;
    roundName: RoundName;
    teams: Team[];
    onBankChange: (bank: number) => void;
    onCurrentTeamIdSet: (teamId: Team['id']) => void;
    onMount: () => void;
};

const successSound = new Audio('/assets/success.mp3');
const failSound = new Audio('/assets/fail.mp3');

export const RoundComponent: React.FunctionComponent<Props> = ({
    answers,
    bank,
    currentQuestion,
    currentTeamId,
    history,
    isSoundSwitchedOn,
    isStarted,
    roundName,
    teams,
    onBankChange,
    onCurrentTeamIdSet,
    onMount,
}) => {
    React.useEffect(() => {
        if (isStarted) {
            onMount();
        } else {
            history.push(TeamNamesRoute.url());
        }
    }, [isStarted, currentQuestion?.id]);

    React.useEffect(() => {
        if (roundName === RoundName.ViceVersa && currentTeamId === undefined) {
            const idOfTeamWithMinPoints = teams.reduce((accumulator, current) =>
                accumulator.points < current.points ? accumulator : current,
            ).id;

            onCurrentTeamIdSet(idOfTeamWithMinPoints);

            return;
        }
    }, [currentTeamId, roundName]);

    React.useEffect(() => {
        onBankChange(bank);

        if (isSoundSwitchedOn && isStarted && bank !== 0) {
            stopAndReplayAudio(successSound);
        }
    }, [bank]);

    React.useEffect(() => {
        if (
            isSoundSwitchedOn &&
            isStarted &&
            !teams.every(({ errors }) => errors === 0)
        ) {
            stopAndReplayAudio(failSound);
        }
    }, [teams[0].errors, teams[1].errors]);

    if (currentQuestion === undefined) {
        return null;
    }

    if (currentTeamId === undefined && roundName !== RoundName.ViceVersa) {
        return (
            <TeamSelectionComponent
                isSoundSwitchedOn={isSoundSwitchedOn}
                teams={teams}
                onCurrentTeamIdSet={onCurrentTeamIdSet}
            />
        );
    }

    return (
        <div
            className={classList({
                container: true,
                [css.component]: true,
            })}
        >
            <header className={css.header}>
                <h2 className={css.title}>
                    {makeFirstUpperCase(currentQuestion.name)}
                </h2>
                <div className={css.scoreboard}>{bank}</div>
            </header>
            {teams.map(({ id, name, points, errors }) => (
                <div className={css.team} key={id}>
                    <div
                        className={classList({
                            [css.teamName]: true,
                            [css.teamIsActive]: id === currentTeamId,
                        })}
                    >
                        {name}
                    </div>
                    <div className={css.scoreboard}>{points}</div>
                    <div className={css.currentRound}>
                        {getRoundSymbolByRoundName(roundName)}
                    </div>
                    <div className={css.teamErrors}>
                        {new Array(3).fill('').map((_, index) => (
                            <div
                                key={index}
                                className={classList({
                                    [css.teamError]: true,
                                    [css.teamErrorActive]: index < errors,
                                })}
                            />
                        ))}
                    </div>
                </div>
            ))}
            <ul className={css.list}>
                {answers
                    .sort((a, b) => a.priority - b.priority)
                    .map(({ id, name, isGuessed, votes, priority }) => (
                        <li
                            key={id}
                            className={classList({
                                [css.answer]: true,
                                [css.answerIsGuessed]: isGuessed,
                            })}
                        >
                            <div className={css.card}>
                                <div
                                    className={classList({
                                        [css.cardSide]: true,
                                        [css.cardFront]: true,
                                    })}
                                >
                                    <span className={css.priorityCircle} />
                                    <span className={css.priority}>
                                        {priority}
                                    </span>
                                    <span className={css.priorityCircle} />
                                </div>
                                <div
                                    className={classList({
                                        [css.cardSide]: true,
                                        [css.cardBack]: true,
                                    })}
                                >
                                    <span>{makeFirstUpperCase(name)}</span>
                                    <span>{votes ?? 0}</span>
                                </div>
                            </div>
                        </li>
                    ))}
            </ul>
        </div>
    );
};
