import * as React from 'react';

import * as css from './RoundComponent.pcss';
import {
    Answer,
    Question,
    Team,
} from '../../../../CorePackage/MainModule/interfaces';
import {
    classList,
    makeFirstUpperCase,
} from '../../../../CorePackage/MainModule/common';
import { RoundName } from '../../enums';
import { ButtonContainer } from '../../containers/host/ButtonsContainer';

export type Props = {
    answers: Answer[];
    bank: number;
    currentQuestion?: Question;
    currentTeamId?: Team['id'];
    roundName: RoundName;
    teams: Team[];
    onAnswerGuess: (answerId: Answer['id']) => void;
    onMount: () => void;
};

export const RoundComponent: React.FunctionComponent<Props> = ({
    answers,
    bank,
    currentQuestion,
    currentTeamId,
    roundName,
    teams,
    onAnswerGuess,
    onMount,
}) => {
    React.useEffect(() => onMount(), [currentQuestion?.id]);

    if (currentQuestion === undefined) {
        return null;
    }

    return (
        <div
            className={classList({
                container: true,
                [css.component]: true,
            })}
        >
            <div className={css.header}>
                <div className={css.roundName}>{roundName}</div>
                <h2 className={css.title}>
                    {makeFirstUpperCase(currentQuestion.name)}
                </h2>
                <h3 className={css.bank}>
                    В Банке: <span>{bank}</span>
                </h3>
                <div className={css.teams}>
                    {teams.map(({ id, name, points, errors }) => (
                        <div
                            className={classList({
                                [css.team]: true,
                                [css.isActive]: id === currentTeamId,
                            })}
                            key={id}
                        >
                            <div className={css.teamName}>{name}</div>
                            <div className={css.teamPoints}>
                                Очки: <span>{points}</span>
                            </div>
                            <div className={css.teamErrors}>
                                Ошибки: <span>{errors}</span>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <div className={css.answers}>
                <h3 className={css.answersTitle}>Варианты ответов:</h3>
                <ul className={css.answersList}>
                    {answers
                        .sort((a, b) => a.priority - b.priority)
                        .map(({ id, name, isGuessed }) => (
                            <li
                                key={id}
                                className={classList({
                                    [css.answer]: true,
                                    [css.isGuessed]: isGuessed,
                                })}
                                onClick={() => {
                                    if (
                                        currentTeamId === undefined ||
                                        isGuessed
                                    ) {
                                        return;
                                    }

                                    onAnswerGuess(id);
                                }}
                            >
                                {makeFirstUpperCase(name)}
                            </li>
                        ))}
                </ul>
            </div>
            <div className={css.buttonsWrapper}>
                <ButtonContainer />
            </div>
        </div>
    );
};
