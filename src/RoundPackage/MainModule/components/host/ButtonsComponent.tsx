import * as React from 'react';

import * as css from './ButtonsComponent.pcss';
import { Team } from '../../../../CorePackage/MainModule/interfaces';
import { QuestionsListRoute } from '../../../../QuestionsListPackage/MainModule/routes';
import { Button } from '../../../../CorePackage/MainModule/components/Button';
import { ButtonType } from '../../../../CorePackage/MainModule/enums';
import { history } from '../../../../CorePackage/MainModule/common';

export type Props = {
    bank: number;
    currentTeamId?: Team['id'];
    isStarted: boolean;
    teams: Team[];
    onCurrentTeamChange: () => void;
    onRoundSwitch: (isStarted: boolean) => void;
    onTeamErrorSubmit: (currentTeamId: Team['id'], errors: number) => void;
    onTeamPointsChange: () => void;
};

export const ButtonsComponent: React.FunctionComponent<Props> = ({
    bank,
    currentTeamId,
    isStarted,
    teams,
    onCurrentTeamChange,
    onRoundSwitch,
    onTeamErrorSubmit,
    onTeamPointsChange,
}) => {
    function handleSwitchRoundButtonClick(
        event: React.MouseEvent<HTMLButtonElement>,
    ) {
        event.preventDefault();

        if (isStarted) {
            if (confirm('Вы уверены, что хотите закончить раунд?')) {
                onRoundSwitch(!isStarted);
            }

            return;
        }

        onRoundSwitch(!isStarted);
    }

    function handleSubmitTeamErrorButtonClick(
        event: React.MouseEvent<HTMLButtonElement>,
    ) {
        event.preventDefault();

        if (currentTeamId === undefined) {
            return;
        }

        const currentTeamErrors = teams.find(team => team.id === currentTeamId)
            ?.errors;

        const newErrors =
            currentTeamErrors === 3 || currentTeamErrors === undefined
                ? 0
                : currentTeamErrors + 1;

        onTeamErrorSubmit(currentTeamId, newErrors);
    }

    function handleBackToQuestionsButtonClick(
        event: React.MouseEvent<HTMLButtonElement>,
    ) {
        event.preventDefault();

        history.push(QuestionsListRoute.url());
    }

    function handleTeamPointsChangeButtonClick() {
        const currentTeamName = teams.find(({ id }) => id === currentTeamId)
            ?.name;

        if (currentTeamName === undefined) {
            return;
        }

        if (confirm(`Начислить ${bank} очков команде ${currentTeamName}?`)) {
            onTeamPointsChange();
        }
    }

    return (
        <div className={css.buttons}>
            <Button
                className={css.button}
                onClick={handleSwitchRoundButtonClick}
            >
                {isStarted ? 'Закончить раунд' : 'Начать раунд'}
            </Button>
            {isStarted ? (
                <Button
                    buttonType={ButtonType.Error}
                    className={css.button}
                    onClick={handleSubmitTeamErrorButtonClick}
                >
                    Подтвердить ошибку
                </Button>
            ) : (
                <Button
                    className={css.button}
                    onClick={handleBackToQuestionsButtonClick}
                >
                    Вернуться к вопросам
                </Button>
            )}
            {isStarted && currentTeamId !== undefined && (
                <>
                    <Button
                        className={css.button}
                        onClick={onCurrentTeamChange}
                    >
                        Сменить команду
                    </Button>
                    <Button
                        buttonType={ButtonType.Success}
                        className={css.button}
                        onClick={handleTeamPointsChangeButtonClick}
                    >
                        Начислить очки команде
                    </Button>
                </>
            )}
        </div>
    );
};
