import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { QuestionsEndpoint } from '../../../CorePackage/ApiModule/endpoints/QuestionsEndpoint';
import { Question } from '../../../CorePackage/MainModule/interfaces';
import { Exception } from '../../../CorePackage/ApiModule/interfaces';

export class FetchRandomQuestionAction implements AsyncAction {
    public static readonly type = 'FETCH_RANDOM_QUESTION';

    public readonly type = FetchRandomQuestionAction.type;
    public readonly payload = QuestionsEndpoint.getRandom().then(
        ({ data }) => data,
    );

    public constructor() {}
}

export class FetchRandomQuestionPendingAction implements Action {
    public static readonly type = 'FETCH_RANDOM_QUESTION_PENDING';

    public readonly type = FetchRandomQuestionPendingAction.type;
}

export class FetchRandomQuestionRejectedAction implements Action {
    public static readonly type = 'FETCH_RANDOM_QUESTION_REJECTED';

    public readonly type = FetchRandomQuestionRejectedAction.type;
    public readonly payload: Exception = {
        error: {
            message: '',
            stack: '',
        },
    };
}

export class FetchRandomQuestionFulfilledAction implements Action {
    public static readonly type = 'FETCH_RANDOM_QUESTION_FULFILLED';

    public readonly type = FetchRandomQuestionFulfilledAction.type;
    public readonly payload: Question = {
        id: '',
        name: '',
    };
}
