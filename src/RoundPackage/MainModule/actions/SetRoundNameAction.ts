import { Action } from 'redux';

import { RoundName } from '../enums';

export class SetRoundNameAction implements Action {
    public static readonly type = 'SET_ROUND_NAME';

    public readonly type = SetRoundNameAction.type;

    public constructor(readonly payload: RoundName) {}
}
