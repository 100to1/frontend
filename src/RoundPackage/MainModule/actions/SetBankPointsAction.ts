import { Action } from 'redux';

export class SetBankPointsAction implements Action {
    public static readonly type = 'SET_BANK_POINTS';

    public readonly type = SetBankPointsAction.type;

    public constructor(readonly payload: number) {}
}
