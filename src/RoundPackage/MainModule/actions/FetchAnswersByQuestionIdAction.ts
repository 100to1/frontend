import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { AnswersEndpoint } from '../../../CorePackage/ApiModule/endpoints/AnswersEndpoint';
import { Answer, Question } from '../../../CorePackage/MainModule/interfaces';
import { createAnswers } from '../../../CorePackage/MainModule/common';
import { Exception } from '../../../CorePackage/ApiModule/interfaces';
import { RoundName } from '../enums';
import { getVotesByRoundName } from '../common';

export class FetchAnswersByQuestionIdAction implements AsyncAction {
    public static readonly type = 'FETCH_ANSWERS_BY_QUESTION_ID';

    public readonly type = FetchAnswersByQuestionIdAction.type;
    public readonly payload = AnswersEndpoint.getByQuestionId(
        this.questionId,
    ).then(({ data }) => {
        let answers = data.map(createAnswers);

        if (this.roundName !== undefined) {
            const votesArray = getVotesByRoundName(this.roundName);
            answers = answers.map((answer, index) => ({
                ...answer,
                votes: votesArray[index],
            }));
        }

        return answers;
    });

    public constructor(
        readonly questionId: Question['id'],
        readonly roundName?: RoundName,
    ) {}
}

export class FetchAnswersByQuestionIdPendingAction implements Action {
    public static readonly type = 'FETCH_ANSWERS_BY_QUESTION_ID_PENDING';

    public readonly type = FetchAnswersByQuestionIdPendingAction.type;
}

export class FetchAnswersByQuestionIdRejectedAction implements Action {
    public static readonly type = 'FETCH_ANSWERS_BY_QUESTION_ID_REJECTED';

    public readonly type = FetchAnswersByQuestionIdRejectedAction.type;
    public readonly payload: Exception = {
        error: {
            message: '',
            stack: '',
        },
    };
}

export class FetchAnswersByQuestionIdFulfilledAction implements Action {
    public static readonly type = 'FETCH_ANSWERS_BY_QUESTION_ID_FULFILLED';

    public readonly type = FetchAnswersByQuestionIdFulfilledAction.type;
    public readonly payload: Answer[] = [];
}
