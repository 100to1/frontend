import { Action } from 'redux';

import { Answer } from '../../../CorePackage/MainModule/interfaces';

export class SetAnswerIsGuessedAction implements Action {
    public static readonly type = 'SET_ANSWER_IS_GUESSED';

    public readonly type = SetAnswerIsGuessedAction.type;
    public constructor(readonly payload: Answer['id'] = '') {}
}
