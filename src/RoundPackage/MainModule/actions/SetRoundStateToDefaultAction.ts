import { Action } from 'redux';

export class SetRoundStateToDefaultAction implements Action {
    public static readonly type = 'SET_ROUND_STATE_TO_DEFAULT';

    public readonly type = SetRoundStateToDefaultAction.type;

    public constructor() {}
}
