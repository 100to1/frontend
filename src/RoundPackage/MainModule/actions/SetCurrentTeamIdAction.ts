import { Action } from 'redux';

import { Team } from '../../../CorePackage/MainModule/interfaces';

export class SetCurrentTeamIdAction implements Action {
    public static readonly type = 'SET_CURRENT_TEAM_ID';

    public readonly type = SetCurrentTeamIdAction.type;

    public constructor(readonly payload: Team['id']) {}
}
