import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { QuestionsEndpoint } from '../../../CorePackage/ApiModule/endpoints/QuestionsEndpoint';
import { Question } from '../../../CorePackage/MainModule/interfaces';
import { Exception } from '../../../CorePackage/ApiModule/interfaces';

export class FetchQuestionByIdAction implements AsyncAction {
    public static readonly type = 'FETCH_QUESTION_BY_ID';

    public readonly type = FetchQuestionByIdAction.type;
    public readonly payload = QuestionsEndpoint.getById(this.questionId).then(
        ({ data }) => data,
    );

    public constructor(readonly questionId: Question['id']) {}
}

export class FetchQuestionByIdPendingAction implements Action {
    public static readonly type = 'FETCH_QUESTION_BY_ID_PENDING';

    public readonly type = FetchQuestionByIdPendingAction.type;
}

export class FetchQuestionByIdRejectedAction implements Action {
    public static readonly type = 'FETCH_QUESTION_BY_ID_REJECTED';

    public readonly type = FetchQuestionByIdRejectedAction.type;
    public readonly payload: Exception = {
        error: {
            message: '',
            stack: '',
        },
    };
}

export class FetchQuestionByIdFulfilledAction implements Action {
    public static readonly type = 'FETCH_QUESTION_BY_ID_FULFILLED';

    public readonly type = FetchQuestionByIdFulfilledAction.type;
    public readonly payload: Question = {
        id: '',
        name: '',
    };
}
