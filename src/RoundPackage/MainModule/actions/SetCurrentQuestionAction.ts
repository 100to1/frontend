import { Action } from 'redux';

import { Question } from '../../../CorePackage/MainModule/interfaces';

export class SetCurrentQuestionAction implements Action {
    public static readonly type = 'SET_CURRENT_QUESTION';

    public readonly type = SetCurrentQuestionAction.type;

    public constructor(readonly payload: Question) {}
}
