import { Action } from 'redux';

export class SetRoundIsStartedAction implements Action {
    public static readonly type = 'SET_ROUND_IS_STARTED';

    public readonly type = SetRoundIsStartedAction.type;
    public constructor(readonly payload: boolean) {}
}
