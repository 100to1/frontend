import { Reducer } from 'redux';

import { State } from '../interfaces';
import { defaultState } from '../common';
import {
    FetchAnswersByQuestionIdAction,
    FetchAnswersByQuestionIdPendingAction,
    FetchAnswersByQuestionIdRejectedAction,
    FetchAnswersByQuestionIdFulfilledAction,
} from '../actions/FetchAnswersByQuestionIdAction';

type TAction =
    | FetchAnswersByQuestionIdAction
    | FetchAnswersByQuestionIdPendingAction
    | FetchAnswersByQuestionIdRejectedAction
    | FetchAnswersByQuestionIdFulfilledAction;

export const fetchAnswersByQuestionIdReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case FetchAnswersByQuestionIdPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case FetchAnswersByQuestionIdRejectedAction.type:
            return {
                ...state,
                isFetching: false,
            };

        case FetchAnswersByQuestionIdFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
                answers: action.payload,
            };

        default:
            return state;
    }
};
