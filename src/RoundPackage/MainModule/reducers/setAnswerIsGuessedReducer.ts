import { Reducer } from 'redux';

import { State } from '../interfaces';
import { defaultState } from '../common';
import { SetAnswerIsGuessedAction } from '../actions/SetAnswerIsGuessedAction';

type TAction = SetAnswerIsGuessedAction;

export const setAnswerIsGuessedReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case SetAnswerIsGuessedAction.type:
            return {
                ...state,
                answers: state.answers.map(answer => {
                    if (answer.id === action.payload) {
                        return {
                            ...answer,
                            isGuessed: true,
                        };
                    }

                    return answer;
                }),

                bank:
                    state.bank +
                    (state.answers.find(answer => answer.id === action.payload)
                        ?.votes ?? 0),
            };

        default:
            return state;
    }
};
