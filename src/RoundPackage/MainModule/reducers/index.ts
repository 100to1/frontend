import reduceReducers from 'reduce-reducers';

import { State } from '../interfaces';
import { defaultState } from '../common';
import { fetchAnswersByQuestionIdReducer } from './fetchAnswersByQuestionIdReducer';
import { fetchQuestionByIdReducer } from './fetchQuestionByIdReducer';
import { setAnswerIsGuessedReducer } from './setAnswerIsGuessedReducer';
import { setRoundIsStartedReducer } from './setRoundIsStartedReducer';
import { setRoundNameReducer } from './setRoundNameReducer';
import { setCurrentTeamIdReducer } from './setCurrentTeamIdReducer';
import { setBankPointsReducer } from './setBankPointsReducer';
import { setCurrentQuestionReducer } from './setCurrentQuestionReducer';
import { setRoundStateToDefaultReducer } from './setRoundStateToDefaultReducer';
import { fetchRandomQuestionReducer } from './fetchRandomQuestionReducer';

export const roundReducer = reduceReducers<State, any>(
    defaultState,
    fetchAnswersByQuestionIdReducer,
    setBankPointsReducer,
    fetchQuestionByIdReducer,
    fetchRandomQuestionReducer,
    setAnswerIsGuessedReducer,
    setCurrentQuestionReducer,
    setCurrentTeamIdReducer,
    setRoundIsStartedReducer,
    setRoundNameReducer,
    setRoundStateToDefaultReducer,
);
