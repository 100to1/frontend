import { Reducer } from 'redux';

import { SetCurrentQuestionAction } from '../actions/SetCurrentQuestionAction';
import { State } from '../interfaces';
import { defaultState } from '../common';

type TAction = SetCurrentQuestionAction;

export const setCurrentQuestionReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case SetCurrentQuestionAction.type:
            return {
                ...state,
                currentQuestion: action.payload,
            };

        default:
            return state;
    }
};
