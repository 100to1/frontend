import { Reducer } from 'redux';

import { SetCurrentTeamIdAction } from '../actions/SetCurrentTeamIdAction';
import { State } from '../interfaces';
import { defaultState } from '../common';

type TAction = SetCurrentTeamIdAction;

export const setCurrentTeamIdReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case SetCurrentTeamIdAction.type:
            return {
                ...state,
                currentTeamId: action.payload,
            };

        default:
            return state;
    }
};
