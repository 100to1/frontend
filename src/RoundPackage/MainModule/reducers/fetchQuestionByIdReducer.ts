import { Reducer } from 'redux';

import { State } from '../interfaces';
import { defaultState } from '../common';
import {
    FetchQuestionByIdAction,
    FetchQuestionByIdFulfilledAction,
    FetchQuestionByIdPendingAction,
    FetchQuestionByIdRejectedAction,
} from '../actions/FetchQuestionByIdAction';

type TAction =
    | FetchQuestionByIdAction
    | FetchQuestionByIdPendingAction
    | FetchQuestionByIdRejectedAction
    | FetchQuestionByIdFulfilledAction;

export const fetchQuestionByIdReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case FetchQuestionByIdPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case FetchQuestionByIdRejectedAction.type:
            return {
                ...state,
                isFetching: false,
            };

        case FetchQuestionByIdFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
                currentQuestion: action.payload,
            };

        default:
            return state;
    }
};
