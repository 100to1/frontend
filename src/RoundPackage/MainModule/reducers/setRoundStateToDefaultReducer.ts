import { Reducer } from 'redux';

import { SetRoundStateToDefaultAction } from '../actions/SetRoundStateToDefaultAction';
import { State } from '../interfaces';
import { defaultState } from '../common';

type TAction = SetRoundStateToDefaultAction;

export const setRoundStateToDefaultReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
) => {
    switch (action.type) {
        case SetRoundStateToDefaultAction.type:
            return defaultState;

        default:
            return state;
    }
};
