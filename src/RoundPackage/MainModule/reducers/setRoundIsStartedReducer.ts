import { Reducer } from 'redux';

import { State } from '../interfaces';
import { defaultState } from '../common';
import { SetRoundIsStartedAction } from '../actions/SetRoundIsStartedAction';

type TAction = SetRoundIsStartedAction;

export const setRoundIsStartedReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case SetRoundIsStartedAction.type:
            return {
                ...state,
                isStarted: action.payload,
            };

        default:
            return state;
    }
};
