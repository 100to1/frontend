import { Reducer } from 'redux';

import { State } from '../interfaces';
import { defaultState } from '../common';
import {
    FetchRandomQuestionAction,
    FetchRandomQuestionFulfilledAction,
    FetchRandomQuestionPendingAction,
    FetchRandomQuestionRejectedAction,
} from '../actions/FetchRandomQuestionAction';

type TAction =
    | FetchRandomQuestionAction
    | FetchRandomQuestionPendingAction
    | FetchRandomQuestionRejectedAction
    | FetchRandomQuestionFulfilledAction;

export const fetchRandomQuestionReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case FetchRandomQuestionPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case FetchRandomQuestionRejectedAction.type:
            return {
                ...state,
                isFetching: false,
            };

        case FetchRandomQuestionFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
                currentQuestion: action.payload,
            };

        default:
            return state;
    }
};
