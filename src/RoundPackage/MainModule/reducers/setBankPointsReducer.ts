import { Reducer } from 'redux';

import { SetBankPointsAction } from '../actions/SetBankPointsAction';
import { State } from '../interfaces';
import { defaultState } from '../common';

type TAction = SetBankPointsAction;

export const setBankPointsReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
) => {
    switch (action.type) {
        case SetBankPointsAction.type:
            return {
                ...state,
                bank: action.payload,
            };

        default:
            return state;
    }
};
