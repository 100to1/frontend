import { Reducer } from 'redux';

import { SetRoundNameAction } from '../actions/SetRoundNameAction';
import { State } from '../interfaces';
import { defaultState } from '../common';

type TAction = SetRoundNameAction;

export const setRoundNameReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case SetRoundNameAction.type:
            return {
                ...state,
                name: action.payload,
            };

        default:
            return state;
    }
};
