import { AbstractRoute, route } from '../../../CorePackage/RouteModule';
import { Question } from '../../../CorePackage/MainModule/interfaces';

type RoundRouteParams = {
    id: Question['id'];
};

@route<RoundRouteParams>()
export class RoundRoute extends AbstractRoute<RoundRouteParams> {
    public static url({ id }: RoundRouteParams = { id: ':id(\\d+)' }) {
        return `/questions/${id}`;
    }
}
