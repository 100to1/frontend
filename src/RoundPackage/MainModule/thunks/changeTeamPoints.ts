import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { RootState } from '../../../CorePackage/MainModule/interfaces';
import { getRoundBank, getRoundCurrentTeamId } from '../selectors';
import { UserRole } from '../../../CorePackage/MainModule/enums';
import { ChangeTeamPointsAction } from '../../../CorePackage/MainModule/actions/SetTeamAction';
import { WebSocketSendMessageAction } from '../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';
import { SetBankPointsAction } from '../actions/SetBankPointsAction';

export function changeTeamPoints(): ThunkAction<
    void,
    RootState,
    void,
    Action
> {
    return (dispatch, getState) => {
        const state = getState();
        const currentTeamId = getRoundCurrentTeamId(state);
        const bank = getRoundBank(state);

        if (currentTeamId == undefined) {
            return;
        }

        const teamData = {
            id: currentTeamId,
            points: bank,
        };

        dispatch(new ChangeTeamPointsAction(teamData));
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: ChangeTeamPointsAction.type,
                    payload: teamData,
                },
                addressee: UserRole.Player,
            }),
        );

        dispatch(new SetBankPointsAction(0));
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetBankPointsAction.type,
                    payload: 0,
                },
                addressee: UserRole.Player,
            }),
        );
    };
}
