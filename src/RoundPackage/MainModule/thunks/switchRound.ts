import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { RootState } from '../../../CorePackage/MainModule/interfaces';
import { SetRoundIsStartedAction } from '../actions/SetRoundIsStartedAction';
import { getRoundCurrentQuestion, getRoundName } from '../selectors';
import { RoundName } from '../enums';
import { SetRoundNameAction } from '../actions/SetRoundNameAction';
import { UserRole } from '../../../CorePackage/MainModule/enums';
import { getTeams } from '../../../CorePackage/MainModule/selectors';
import { SubmitTeamErrorAction } from '../../../CorePackage/MainModule/actions/SubmitTeamErrorAction';
import { WebSocketSendMessageAction } from '../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';
import { SetCurrentQuestionAction } from '../actions/SetCurrentQuestionAction';
import { SetRoundStateToDefaultAction } from '../actions/SetRoundStateToDefaultAction';
import { history } from '../../../CorePackage/MainModule/common';
import { QuestionsListRoute } from '../../../QuestionsListPackage/MainModule/routes';

export function switchRound(
    isStarted: boolean,
): ThunkAction<void, RootState, void, Action> {
    return (dispatch, getState) => {
        dispatch(new SetRoundIsStartedAction(isStarted));
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetRoundIsStartedAction.type,
                    payload: isStarted,
                },
                addressee: UserRole.Player,
            }),
        );

        const state = getState();

        if (isStarted) {
            const currentQuestion = getRoundCurrentQuestion(state);

            if (currentQuestion === undefined) {
                return;
            }

            dispatch(
                new WebSocketSendMessageAction({
                    action: {
                        type: SetCurrentQuestionAction.type,
                        payload: currentQuestion,
                    },
                    addressee: UserRole.Player,
                }),
            );

            return;
        }

        dispatch(new SetRoundStateToDefaultAction());
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetRoundStateToDefaultAction.type,
                },
                addressee: UserRole.Player,
            }),
        );

        const currentRoundName = getRoundName(state);
        const newRoundName =
            currentRoundName === RoundName.Simple
                ? RoundName.Double
                : currentRoundName === RoundName.Double
                ? RoundName.Triple
                : currentRoundName === RoundName.Triple
                ? RoundName.ViceVersa
                : RoundName.Simple;

        dispatch(new SetRoundNameAction(newRoundName));
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetRoundNameAction.type,
                    payload: newRoundName,
                },
                addressee: UserRole.Player,
            }),
        );

        const teams = getTeams(state);
        teams.map(({ id }) => {
            const teamData = {
                teamId: id,
                errors: 0,
            };

            dispatch(new SubmitTeamErrorAction(teamData));
            dispatch(
                new WebSocketSendMessageAction({
                    action: {
                        type: SubmitTeamErrorAction.type,
                        payload: teamData,
                    },
                    addressee: UserRole.Player,
                }),
            );
        });

        history.push(QuestionsListRoute.url());
    };
}
