import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import {
    Question,
    RootState,
} from '../../../CorePackage/MainModule/interfaces';
import { getRoundName } from '../selectors';
import { FetchAnswersByQuestionIdAction } from '../actions/FetchAnswersByQuestionIdAction';

export function fetchAnswers(
    questionId: Question['id'],
): ThunkAction<void, RootState, void, Action> {
    return (dispatch, getState) => {
        const state = getState();
        const roundName = getRoundName(state);

        dispatch(new FetchAnswersByQuestionIdAction(questionId, roundName));
    };
}
