import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { RootState } from '../../../CorePackage/MainModule/interfaces';
import { getTeams } from '../../../CorePackage/MainModule/selectors';
import { getRoundCurrentTeamId } from '../selectors';
import { SetCurrentTeamIdAction } from '../actions/SetCurrentTeamIdAction';
import { UserRole } from '../../../CorePackage/MainModule/enums';
import { WebSocketSendMessageAction } from '../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';

export function changeCurrentTeam(): ThunkAction<
    void,
    RootState,
    void,
    Action
> {
    return (dispatch, getState) => {
        const state = getState();
        const teams = getTeams(state);
        const currentTeamId = getRoundCurrentTeamId(state);

        const newPlayerTeam = teams.find(({ id }) => id !== currentTeamId);

        if (newPlayerTeam === undefined) {
            return;
        }

        dispatch(new SetCurrentTeamIdAction(newPlayerTeam.id));
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetCurrentTeamIdAction.type,
                    payload: newPlayerTeam.id,
                },
                addressee: UserRole.Player,
            }),
        );
    };
}
