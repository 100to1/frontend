import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import * as css from './IndexComponent.pcss';
import { classList } from '../../../../CorePackage/MainModule/common';
import { TextField } from '../../../../CorePackage/MainModule/components/TextField';
import { Button } from '../../../../CorePackage/MainModule/components/Button';

export type Props = RouteComponentProps & {
    isHostConnected: boolean;
    isPlayerConnected: boolean;
    onHostConnect: () => void;
    onPlayerConnect: () => void;
    onSubmit: (newConnectionCode: string) => void;
};

export const IndexComponent: React.FunctionComponent<Props> = ({
    isHostConnected,
    isPlayerConnected,
    onHostConnect,
    onPlayerConnect,
    onSubmit,
}) => {
    React.useEffect(() => {
        if (isPlayerConnected) {
            onPlayerConnect();
        }
    }, [isPlayerConnected]);

    React.useEffect(() => {
        if (isHostConnected) {
            onHostConnect();
        }
    }, [isHostConnected]);

    const [value, setValue] = React.useState('');

    function handleValueChange(event: React.ChangeEvent<HTMLInputElement>) {
        event.preventDefault();

        setValue(event.currentTarget.value);
    }

    function handleSubmit(event: React.MouseEvent<HTMLButtonElement>) {
        event.preventDefault();

        onSubmit(value);
    }

    return (
        <div
            className={classList({
                container: true,
                [css.component]: true,
            })}
        >
            <form className={css.form}>
                <TextField
                    label="Введите код синхронизации"
                    value={value}
                    className={css.textField}
                    onChange={handleValueChange}
                />
                <Button onClick={handleSubmit}>Подтвердить</Button>
            </form>
        </div>
    );
};
