import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import * as css from './IndexComponent.pcss';
import { classList } from '../../../../CorePackage/MainModule/common';

export type Props = RouteComponentProps & {
    connectionCode?: string;
    isPlayerConnected: boolean;
    onPlayerConnect: () => void;
    onMount: () => void;
};

export const IndexComponent: React.FunctionComponent<Props> = ({
    connectionCode,
    isPlayerConnected,
    onPlayerConnect,
    onMount,
}) => {
    React.useEffect(() => {
        if (connectionCode === undefined) {
            onMount();
        }
    });

    React.useEffect(() => {
        if (isPlayerConnected) {
            onPlayerConnect();
        }
    }, [isPlayerConnected]);

    return (
        <div
            className={classList({
                container: true,
                [css.component]: true,
            })}
        >
            <p className={css.text}>Код синхронизации: {connectionCode}</p>
        </div>
    );
};
