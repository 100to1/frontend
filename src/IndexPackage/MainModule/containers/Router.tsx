import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { IndexRoute } from '../routes/';
import { UserRole } from '../../../CorePackage/MainModule/enums';
import { IndexContainer as PlayerIndexPage } from './player/IndexContainer';
import { IndexContainer as HostIndexPage } from './host/IndexContainer';

type Props = RouteProps & {
    userRole: UserRole;
};

export class Router extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: IndexRoute.url(),
    };

    public render() {
        const { userRole } = this.props;

        return (
            <Route
                {...this.props}
                component={
                    userRole === UserRole.Host ? HostIndexPage : PlayerIndexPage
                }
            />
        );
    }
}
