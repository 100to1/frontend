import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';

import {
    IndexComponent,
    Props as IndexComponentProps,
} from '../../components/host/IndexComponent';
import { RootState } from '../../../../CorePackage/MainModule/interfaces';
import {
    getConnectionCode,
    getIsPlayerConnected,
} from '../../../../CorePackage/MainModule/selectors';
import { SetConnectionCodeAction } from '../../../../CorePackage/MainModule/actions/SetConnectionCodeAction';
import { UserRole } from '../../../../CorePackage/MainModule/enums';
import { WebSocketConnectAction } from '../../../../CorePackage/WebSocketModule/actions/WebSocketConnectionActions';
import { WebSocketSendMessageAction } from '../../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';
import { SetIsHostConnectedAction } from '../../../../CorePackage/MainModule/actions/SetIsHostConnectedAction';
import { TeamNamesRoute } from '../../../../TeamNamesPackage/MainModule/routes';

type Props = IndexComponentProps;

type StateProps = Pick<Props, 'connectionCode' | 'isPlayerConnected'>;
type DispatchProps = Pick<Props, 'onPlayerConnect' | 'onMount'>;
type OwnProps = Pick<Props, 'history'>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    connectionCode: getConnectionCode(state),
    isPlayerConnected: getIsPlayerConnected(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch,
    ownProps,
) => ({
    onMount: () => {
        const newConnectionCode = String(Math.round(Math.random() * 1000000));

        dispatch(new SetConnectionCodeAction(newConnectionCode));
        dispatch(
            new WebSocketConnectAction(
                `${process.env.WS_SERVER}?connectionCode=${newConnectionCode}`,
            ),
        );
    },
    onPlayerConnect: () => {
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetIsHostConnectedAction.type,
                    payload: true,
                },
                addressee: UserRole.Player,
            }),
        );
        ownProps.history.push(TeamNamesRoute.url());
    },
});

export const IndexContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(IndexComponent);
