import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';

import {
    IndexComponent,
    Props as IndexComponentProps,
} from '../../components/player/IndexComponent';
import { RootState } from '../../../../CorePackage/MainModule/interfaces';
import {
    getIsHostConnected,
    getIsPlayerConnected,
} from '../../../../CorePackage/MainModule/selectors';
import { SetConnectionCodeAction } from '../../../../CorePackage/MainModule/actions/SetConnectionCodeAction';
import { UserRole } from '../../../../CorePackage/MainModule/enums';
import { WebSocketConnectAction } from '../../../../CorePackage/WebSocketModule/actions/WebSocketConnectionActions';
import { WebSocketSendMessageAction } from '../../../../CorePackage/WebSocketModule/actions/WebSocketSendMessageAction';
import { SetIsPlayerConnectedAction } from '../../../../CorePackage/MainModule/actions/SetIsPlayerConnectedAction';
import { TeamNamesRoute } from '../../../../TeamNamesPackage/MainModule/routes';

type Props = IndexComponentProps;

type StateProps = Pick<Props, 'isHostConnected' | 'isPlayerConnected'>;
type DispatchProps = Pick<
    Props,
    'onHostConnect' | 'onPlayerConnect' | 'onSubmit'
>;
type OwnProps = Pick<Props, 'history'>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    isHostConnected: getIsHostConnected(state),
    isPlayerConnected: getIsPlayerConnected(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch,
    ownProps,
) => ({
    onHostConnect: () => {
        ownProps.history.push(TeamNamesRoute.url());
    },
    onPlayerConnect: () => {
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SetIsPlayerConnectedAction.type,
                    payload: true,
                },
                addressee: UserRole.Host,
            }),
        );
    },
    onSubmit: newConnectionCode => {
        dispatch(new SetConnectionCodeAction(newConnectionCode));
        dispatch(
            new WebSocketConnectAction(
                `${process.env.WS_SERVER}?connectionCode=${newConnectionCode}`,
            ),
        );
    },
});

export const IndexContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(IndexComponent);
