import { AbstractRoute, route } from '../../../CorePackage/RouteModule';

@route()
export class IndexRoute extends AbstractRoute {
    public static url() {
        return '/';
    }
}
