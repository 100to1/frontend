import { ThunkAction } from 'redux-thunk';

import { RootState } from '../../../CorePackage/MainModule/interfaces';
import { FetchQuestionsAction } from '../actions/FetchQuestionsAction';
import { getQuestionsListPagination } from '../selectors';

export function fetchQuestions(): ThunkAction<
    void,
    RootState,
    void,
    FetchQuestionsAction
> {
    return (dispatch, getState) => {
        const state = getState();
        const { limit, offset } = getQuestionsListPagination(state);

        return dispatch(new FetchQuestionsAction(limit, offset));
    };
}
