import { ThunkAction } from 'redux-thunk';

import { RootState } from '../../../CorePackage/MainModule/interfaces';
import { FetchRandomQuestionAction } from '../../../RoundPackage/MainModule/actions/FetchRandomQuestionAction';
import { getRoundCurrentQuestion } from '../../../RoundPackage/MainModule/selectors';
import { history } from '../../../CorePackage/MainModule/common';
import { RoundRoute } from '../../../RoundPackage/MainModule/routes';

export function fetchRandomQuestion(): ThunkAction<
    void,
    RootState,
    void,
    FetchRandomQuestionAction
> {
    return async (dispatch, getState) => {
        await dispatch(new FetchRandomQuestionAction());

        const state = getState();
        const currentQuestionId = getRoundCurrentQuestion(state)?.id;

        if (currentQuestionId === undefined) {
            return;
        }

        history.push(RoundRoute.url({ id: currentQuestionId }));
    };
}
