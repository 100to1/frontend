import reduceReducers from 'reduce-reducers';

import { State } from '../interfaces';
import { defaultState } from '../common';
import { fetchQuestionsReducer } from './fetchQuestionsReducer';
import { paginationReducer } from './paginationReducer';

export const questionsListReducer = reduceReducers<State, any>(
    defaultState,
    fetchQuestionsReducer,
    paginationReducer,
);
