import { Reducer } from 'redux';

import { State } from '../interfaces';
import { defaultState } from '../common';
import {
    FetchQuestionsAction,
    FetchQuestionsPendingAction,
    FetchQuestionsRejectedAction,
    FetchQuestionsFulfilledAction,
} from '../actions/FetchQuestionsAction';

type TAction =
    | FetchQuestionsAction
    | FetchQuestionsPendingAction
    | FetchQuestionsRejectedAction
    | FetchQuestionsFulfilledAction;

export const fetchQuestionsReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case FetchQuestionsPendingAction.type:
            return {
                ...state,
                isFetching: true,
            };

        case FetchQuestionsRejectedAction.type:
            return {
                ...state,
                isFetching: false,
            };

        case FetchQuestionsFulfilledAction.type:
            return {
                ...state,
                isFetching: false,
                questions: action.payload.data,
                pagination: Object.assign(
                    {},
                    state.pagination,
                    action.payload.meta,
                ),
            };

        default:
            return state;
    }
};
