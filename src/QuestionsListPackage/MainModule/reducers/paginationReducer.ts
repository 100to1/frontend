import { Reducer } from 'redux';

import { SetPaginationOffsetAction } from '../actions/SetPaginationOffsetAction';
import { State } from '../interfaces';
import { defaultState } from '../common';

type TAction = SetPaginationOffsetAction;

export const paginationReducer: Reducer<State, TAction> = (
    state = defaultState,
    action,
): State => {
    switch (action.type) {
        case SetPaginationOffsetAction.type:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    offset: action.offset,
                },
            };

        default:
            return state;
    }
};
