import {
    Pagination,
    Question,
} from '../../../CorePackage/MainModule/interfaces';

export interface State {
    isFetching: boolean;
    questions: Question[];
    pagination: Pagination;
}
