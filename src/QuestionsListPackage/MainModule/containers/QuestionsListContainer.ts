import { Action } from 'redux';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import {
    Props as QuestionsListProps,
    QuestionsList,
} from '../components/QuestionsList';
import { RootState } from '../../../CorePackage/MainModule/interfaces';
import {
    getQuestionsListIsFetching,
    getQuestionsListPagination,
    getQuestionsListQuestions,
} from '../selectors';
import { fetchQuestions, fetchRandomQuestion } from '../thunks';
import { getRoundName } from '../../../RoundPackage/MainModule/selectors';

type Props = QuestionsListProps;
type StateProps = Pick<
    Props,
    'isFetching' | 'pagination' | 'questions' | 'roundName'
>;
type DispatchProps = Pick<Props, 'onMount' | 'onRandomQuestionChoose'>;
type OwnProps = Pick<QuestionsListProps, never>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    isFetching: getQuestionsListIsFetching(state),
    pagination: getQuestionsListPagination(state),
    questions: getQuestionsListQuestions(state),
    roundName: getRoundName(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch: ThunkDispatch<RootState, void, Action>,
) => ({
    onMount: () => {
        dispatch(fetchQuestions());
    },
    onRandomQuestionChoose: () => {
        dispatch(fetchRandomQuestion());
    },
});

export const QuestionsListContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(QuestionsList);
