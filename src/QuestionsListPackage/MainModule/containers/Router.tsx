import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { QuestionsListRoute } from '../routes';
import { QuestionsListContainer } from './QuestionsListContainer';

type Props = RouteProps & {};

export class Router extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: QuestionsListRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={QuestionsListContainer} />;
    }
}
