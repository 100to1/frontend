import { Action } from 'redux';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import { RootState } from '../../../CorePackage/MainModule/interfaces';
import { Pagination } from '../../../CorePackage/MainModule/components/Pagination';
import { SetPaginationOffsetAction } from '../actions/SetPaginationOffsetAction';
import { getQuestionsListPagination } from '../selectors';

type StateProps = Pick<
    Pagination['props'],
    'from' | 'to' | 'limit' | 'offset' | 'total'
>;
type OwnProps = {};
type DispatchProps = Pick<Pagination['props'], 'onPageChange'>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => {
    const { limit, offset, total } = getQuestionsListPagination(state);
    const from = offset + 1;
    const to = offset + limit;

    return {
        from: total === 0 ? 0 : from,
        to: to > total ? total : to,
        limit: limit,
        offset: offset,
        total: total,
    };
};

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
    dispatch: ThunkDispatch<RootState, void, Action>,
) => ({
    onPageChange: offset => {
        dispatch(new SetPaginationOffsetAction(offset));
    },
});

export const PaginationContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Pagination);
