import { AbstractRoute, route } from '../../../CorePackage/RouteModule';

@route()
export class QuestionsListRoute extends AbstractRoute {
    public static url() {
        return '/questions';
    }
}
