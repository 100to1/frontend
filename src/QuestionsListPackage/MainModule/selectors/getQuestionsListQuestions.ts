import { createSelector } from 'reselect';
import { getQuestionsList } from './getQuestionsList';

export const getQuestionsListQuestions = createSelector(
    getQuestionsList,
    questionsList => questionsList.questions,
);
