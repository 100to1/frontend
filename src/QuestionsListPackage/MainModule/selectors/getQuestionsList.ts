import { createSelector } from 'reselect';
import { RootState } from '../../../CorePackage/MainModule/interfaces';

export const getQuestionsList = createSelector(
    (state: RootState) => state,
    state => state.QuestionsList,
);
