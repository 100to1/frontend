import { createSelector } from 'reselect';
import { getQuestionsList } from './getQuestionsList';

export const getQuestionsListPagination = createSelector(
    getQuestionsList,
    questionsList => questionsList.pagination,
);
