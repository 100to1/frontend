import { createSelector } from 'reselect';
import { getQuestionsList } from './getQuestionsList';

export const getQuestionsListIsFetching = createSelector(
    getQuestionsList,
    questionsList => questionsList.isFetching,
);
