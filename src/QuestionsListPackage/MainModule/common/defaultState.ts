import { State } from '../interfaces';

export const defaultState: State = {
    isFetching: false,
    questions: [],
    pagination: {
        limit: 20,
        offset: 0,
        total: 0,
    },
};
