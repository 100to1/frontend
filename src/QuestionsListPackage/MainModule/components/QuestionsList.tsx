import * as React from 'react';
import { Link } from 'react-router-dom';

import * as css from './QuestionsList.pcss';
import {
    Pagination,
    Question,
} from '../../../CorePackage/MainModule/interfaces';
import { PaginationContainer } from '../containers/PaginationContainer';
import { RoundRoute } from '../../../RoundPackage';
import {
    classList,
    makeFirstUpperCase,
} from '../../../CorePackage/MainModule/common';
import { RoundName } from '../../../RoundPackage/MainModule/enums';
import { Button } from '../../../CorePackage/MainModule/components/Button';

export type Props = {
    isFetching: boolean;
    pagination: Pagination;
    questions: Question[];
    roundName: RoundName;
    onMount: () => void;
    onRandomQuestionChoose: () => void;
};

export const QuestionsList: React.FunctionComponent<Props> = ({
    isFetching,
    pagination,
    questions,
    roundName,
    onMount,
    onRandomQuestionChoose,
}) => {
    React.useEffect(() => {
        onMount();
    }, [pagination.offset]);

    if (isFetching) {
        return null;
    }

    return (
        <div
            className={classList({
                container: true,
                [css.component]: true,
            })}
        >
            <header className={css.header}>
                <h4 className={css.roundName}>Следующий раунд: {roundName}</h4>
                <h2 className={css.title}>Выберите вопрос</h2>
            </header>
            <ul className={css.list}>
                {questions.map(({ id, name }) => (
                    <li className={css.listItem} key={id}>
                        <span className={css.number}>{id}</span>
                        <Link to={RoundRoute.url({ id: id })}>
                            {makeFirstUpperCase(name)}
                        </Link>
                    </li>
                ))}
            </ul>
            <div className={css.random}>
                <Button
                    className={css.randomButton}
                    onClick={onRandomQuestionChoose}
                >
                    Выбрать случайный вопрос
                </Button>
            </div>
            <div className={css.pagination}>
                <PaginationContainer />
            </div>
        </div>
    );
};
