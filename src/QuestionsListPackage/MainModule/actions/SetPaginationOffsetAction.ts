import { Action } from 'redux';
import { Pagination } from '../../../CorePackage/MainModule/interfaces';

export class SetPaginationOffsetAction implements Action {
    public static readonly type = 'QUESTIONS_LIST_SET_PAGINATION_OFFSET';

    public readonly type = SetPaginationOffsetAction.type;

    public constructor(readonly offset: Pagination['offset']) {}
}
