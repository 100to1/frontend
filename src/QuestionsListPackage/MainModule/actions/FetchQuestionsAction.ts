import { Action } from 'redux';
import { AsyncAction } from 'redux-promise-middleware';

import { QuestionsEndpoint } from '../../../CorePackage/ApiModule/endpoints/QuestionsEndpoint';
import {
    Pagination,
    Question,
} from '../../../CorePackage/MainModule/interfaces';
import {
    ApiResponse,
    Exception,
} from '../../../CorePackage/ApiModule/interfaces';

export class FetchQuestionsAction implements AsyncAction {
    public static readonly type = 'FETCH_QUESTIONS';

    public readonly type = FetchQuestionsAction.type;
    public readonly payload = QuestionsEndpoint.getQuestions(
        this.limit,
        this.offset,
    ).then(response => response);

    public constructor(
        readonly limit: Pagination['limit'],
        readonly offset: Pagination['offset'],
    ) {}
}

export class FetchQuestionsPendingAction implements Action {
    public static readonly type = 'FETCH_QUESTIONS_PENDING';

    public readonly type = FetchQuestionsPendingAction.type;
}

export class FetchQuestionsRejectedAction implements Action {
    public static readonly type = 'FETCH_QUESTIONS_REJECTED';

    public readonly type = FetchQuestionsRejectedAction.type;
    public readonly payload: Exception = {
        error: {
            message: '',
            stack: '',
        },
    };
}

export class FetchQuestionsFulfilledAction implements Action {
    public static readonly type = 'FETCH_QUESTIONS_FULFILLED';

    public readonly type = FetchQuestionsFulfilledAction.type;
    public readonly payload: ApiResponse<Question[]> = {
        data: [],
    };
}
