import { RouteComponentProps } from 'react-router';

export interface AppRoute<P> {
    props: RouteComponentProps<P>;
}
