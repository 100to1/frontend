import { AppRoute } from './AppRoute';

export interface RouteStatic<P> {
    new (...args: unknown[]): AppRoute<P>;

    url(params: P): string;
}
