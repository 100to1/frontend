export function stopAndReplayAudio(audioElement: HTMLAudioElement) {
    if (!audioElement.ended) {
        audioElement.pause();
        audioElement.currentTime = 0;
    }

    audioElement.play();
}
