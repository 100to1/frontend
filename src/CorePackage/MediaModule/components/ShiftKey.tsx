import React from 'react';

export const ShiftKey: React.FunctionComponent = () => {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            width="208"
            height="72"
            version="1.1"
        >
            <defs>
                <linearGradient id="f">
                    <stop offset="0" stopColor="#d8d8d8" stopOpacity="1"></stop>
                    <stop
                        offset="0.478"
                        stopColor="#d8d8d8"
                        stopOpacity="1"
                    ></stop>
                    <stop offset="1" stopColor="#b1b1b1" stopOpacity="1"></stop>
                </linearGradient>
                <linearGradient id="d">
                    <stop offset="0" stopColor="#454545" stopOpacity="1"></stop>
                    <stop
                        offset="0.973"
                        stopColor="#565656"
                        stopOpacity="1"
                    ></stop>
                    <stop offset="1" stopColor="#fff" stopOpacity="1"></stop>
                </linearGradient>
                <linearGradient id="c">
                    <stop offset="0" stopColor="#bfbfbf" stopOpacity="1"></stop>
                    <stop
                        offset="0.478"
                        stopColor="#a6a6a6"
                        stopOpacity="1"
                    ></stop>
                    <stop offset="1" stopColor="#838383" stopOpacity="1"></stop>
                </linearGradient>
                <linearGradient id="b">
                    <stop offset="0" stopColor="#bfbfbf" stopOpacity="1"></stop>
                    <stop
                        offset="0.478"
                        stopColor="#a6a6a6"
                        stopOpacity="0.471"
                    ></stop>
                    <stop offset="1" stopColor="#838383" stopOpacity="1"></stop>
                </linearGradient>
                <linearGradient id="a">
                    <stop offset="0" stopColor="#959595" stopOpacity="1"></stop>
                    <stop
                        offset="0.973"
                        stopColor="#565656"
                        stopOpacity="1"
                    ></stop>
                    <stop offset="1" stopColor="#fff" stopOpacity="1"></stop>
                </linearGradient>
                <linearGradient
                    x1="10.288"
                    x2="193.918"
                    y1="3.98"
                    y2="3.98"
                    gradientUnits="userSpaceOnUse"
                    xlinkHref="#a"
                ></linearGradient>
                <linearGradient
                    x1="10.012"
                    x2="194.187"
                    y1="29.367"
                    y2="29.367"
                    gradientUnits="userSpaceOnUse"
                    xlinkHref="#b"
                ></linearGradient>
                <linearGradient
                    x1="10.012"
                    x2="194.187"
                    y1="29.367"
                    y2="29.367"
                    gradientUnits="userSpaceOnUse"
                    xlinkHref="#c"
                ></linearGradient>
                <linearGradient
                    x1="10.288"
                    x2="193.918"
                    y1="3.98"
                    y2="3.98"
                    gradientUnits="userSpaceOnUse"
                    xlinkHref="#a"
                ></linearGradient>
                <linearGradient
                    x1="10.288"
                    x2="193.918"
                    y1="3.98"
                    y2="3.98"
                    gradientTransform="matrix(1.11027 0 0 1.03226 -8.913 -1.165)"
                    gradientUnits="userSpaceOnUse"
                    xlinkHref="#d"
                ></linearGradient>
                <clipPath id="g" clipPathUnits="userSpaceOnUse">
                    <use width="1" height="1" x="0" y="0" xlinkHref="#e"></use>
                </clipPath>
                <filter
                    id="i"
                    width="1.066"
                    height="1.414"
                    x="-0.033"
                    y="-0.207"
                    colorInterpolationFilters="sRGB"
                >
                    <feGaussianBlur stdDeviation="2.809"></feGaussianBlur>
                </filter>
                <filter
                    id="h"
                    width="1.247"
                    height="1.081"
                    x="-0.123"
                    y="-0.041"
                    colorInterpolationFilters="sRGB"
                >
                    <feGaussianBlur stdDeviation="1.334"></feGaussianBlur>
                </filter>
                <linearGradient
                    id="j"
                    x1="10.012"
                    x2="194.187"
                    y1="29.367"
                    y2="29.367"
                    gradientUnits="userSpaceOnUse"
                    xlinkHref="#f"
                ></linearGradient>
                <linearGradient
                    id="k"
                    x1="10.288"
                    x2="193.918"
                    y1="3.98"
                    y2="3.98"
                    gradientUnits="userSpaceOnUse"
                    xlinkHref="#a"
                ></linearGradient>
            </defs>
            <g
                color="#000"
                transform="translate(-.445 -.127)"
                visibility="visible"
            >
                <g
                    fillOpacity="1"
                    stroke="none"
                    strokeWidth="1.5"
                    clip-path="url(#g)"
                >
                    <path
                        fill="#656565"
                        fillRule="nonzero"
                        d="M-.813-7.188l.407 2.25L-5.531-.03l-2.75 70.094L9.935 82.499 27.86 49.096l-8.08-34.69 171.188-2 24.812-16z"
                        display="inline"
                        enableBackground="accumulate"
                        overflow="visible"
                        style={{ marker: 'none' }}
                    ></path>
                    <path
                        fill="#d6d6d6"
                        fillRule="nonzero"
                        d="M187.918 5.497c3.863-.827 28.146-6.07 28.146-6.07L215.788 72l-12.417 6.899-16.28-24.283z"
                        display="inline"
                        enableBackground="accumulate"
                        filter="url(#h)"
                        overflow="visible"
                        style={{ marker: 'none' }}
                    ></path>
                    <path
                        fill="#939393"
                        d="M3.587 74.76l12.97-26.767 167.498-2.76L205.855 72l1.103 18.212-209.441 3.312z"
                        display="inline"
                        enableBackground="accumulate"
                        filter="url(#i)"
                        overflow="visible"
                        style={{ marker: 'none' }}
                    ></path>
                </g>
                <rect
                    id="e"
                    width="202.819"
                    height="70.642"
                    x="3.035"
                    y="0.806"
                    fill="none"
                    stroke="#727272"
                    strokeOpacity="1"
                    strokeWidth="1.5"
                    display="inline"
                    enableBackground="accumulate"
                    overflow="visible"
                    rx="6.071"
                    style={{ marker: 'none' }}
                    ry="6.071"
                ></rect>
                <rect
                    width="182.675"
                    height="54.913"
                    x="10.762"
                    y="1.91"
                    fill="url(#j)"
                    fillOpacity="1"
                    stroke="url(#k)"
                    strokeWidth="1"
                    display="inline"
                    enableBackground="accumulate"
                    overflow="visible"
                    rx="6.071"
                    style={{ marker: 'none' }}
                    ry="6.071"
                ></rect>
                <path
                    fill="none"
                    stroke="#4d4d4d"
                    strokeWidth="1.5"
                    style={{ marker: 'none' }}
                    d="M28.878 27.317h-4.683l9.951-9.951 10.342 10.341H39.61v6.44H28.488z"
                    display="inline"
                    enableBackground="accumulate"
                    overflow="visible"
                ></path>
                <text
                    x="51.317"
                    y="40.585"
                    fill="#4d4d4d"
                    stroke="none"
                    strokeWidth="1.5"
                    display="inline"
                    enableBackground="accumulate"
                    fontFamily="Arial"
                    style={{
                        textAlign: 'start',
                        marker: 'none',
                    }}
                    fontSize="12"
                    fontStretch="normal"
                    fontStyle="normal"
                    fontVariant="normal"
                    fontWeight="normal"
                    overflow="visible"
                    textAnchor="start"
                    xmlSpace="preserve"
                >
                    <tspan x="51.317" y="40.585">
                        Shift
                    </tspan>
                </text>
            </g>
        </svg>
    );
};
