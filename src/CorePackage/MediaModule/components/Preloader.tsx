import * as React from 'react';

export const Preloader: React.FunctionComponent = () => (
    <svg version="1.0" width="64px" height="64px" viewBox="0 0 128 128">
        <g>
            <path d="M59.6 0h8v40h-8V0z" fill="#fdd701" />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fff7cc"
                transform="rotate(30 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fff7cc"
                transform="rotate(60 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fff7cc"
                transform="rotate(90 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fff7cc"
                transform="rotate(120 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fef3b2"
                transform="rotate(150 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#feef99"
                transform="rotate(180 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#feeb80"
                transform="rotate(210 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fee767"
                transform="rotate(240 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fee34d"
                transform="rotate(270 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fddf34"
                transform="rotate(300 64 64)"
            />
            <path
                d="M59.6 0h8v40h-8V0z"
                fill="#fddb1a"
                transform="rotate(330 64 64)"
            />
            <animateTransform
                attributeName="transform"
                type="rotate"
                values="0 64 64;30 64 64;60 64 64;90 64 64;120 64 64;150 64 64;180 64 64;210 64 64;240 64 64;270 64 64;300 64 64;330 64 64"
                calcMode="discrete"
                dur="1200ms"
                repeatCount="indefinite"
            ></animateTransform>
        </g>
    </svg>
);
