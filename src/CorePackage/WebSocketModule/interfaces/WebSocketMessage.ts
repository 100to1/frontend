import { Action } from 'redux';

import { UserRole } from '../../MainModule/enums';

export interface WebSocketMessage {
    action: Action & { payload?: any };
    addressee?: UserRole;
}
