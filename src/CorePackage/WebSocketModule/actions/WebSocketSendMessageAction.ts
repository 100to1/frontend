import { Action } from 'redux';

import { WebSocketMessage } from '../interfaces';

export class WebSocketSendMessageAction implements Action {
    public static readonly type = 'WS_SEND_MESSAGE';

    public readonly type = WebSocketSendMessageAction.type;

    public constructor(readonly message: WebSocketMessage) {}
}
