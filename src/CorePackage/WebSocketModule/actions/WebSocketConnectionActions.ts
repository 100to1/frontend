import { Action } from 'redux';

export class WebSocketConnectAction implements Action {
    public static readonly type = 'WS_CONNECT';

    public readonly type = WebSocketConnectAction.type;

    public constructor(readonly host: string) {}
}

export class WebSocketConnectingAction implements Action {
    public static readonly type = 'WS_CONNECTING';

    public readonly type = WebSocketConnectingAction.type;

    public constructor() {}
}

export class WebSocketConnectedAction implements Action {
    public static readonly type = 'WS_CONNECTED';

    public readonly type = WebSocketConnectedAction.type;

    public constructor() {}
}

export class WebSocketDisconnectAction implements Action {
    public static readonly type = 'WS_DISCONNECT';

    public readonly type = WebSocketDisconnectAction.type;

    public constructor() {}
}

export class WebSocketDisconnectedAction implements Action {
    public static readonly type = 'WS_DISCONNECTED';

    public readonly type = WebSocketDisconnectedAction.type;

    public constructor() {}
}
