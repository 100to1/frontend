import { Reducer } from 'redux';

import {
    WebSocketConnectAction,
    WebSocketConnectedAction,
    WebSocketConnectingAction,
    WebSocketDisconnectAction,
    WebSocketDisconnectedAction,
} from '../actions/WebSocketConnectionActions';
import { AppState } from '../../MainModule/interfaces';
import { defaultState } from '../../MainModule/common';
import { UserRole } from '../../MainModule/enums';

type TAction =
    | WebSocketConnectAction
    | WebSocketConnectingAction
    | WebSocketConnectedAction
    | WebSocketDisconnectAction
    | WebSocketDisconnectedAction;

export const webSocketConnectionReducer: Reducer<AppState, TAction> = (
    state = defaultState,
    action,
): AppState => {
    const whatToConnect =
        state.userRole === UserRole.Host
            ? 'isHostConnected'
            : 'isPlayerConnected';

    switch (action.type) {
        case WebSocketConnectedAction.type:
            return {
                ...state,
                [whatToConnect]: true,
            };

        case WebSocketDisconnectedAction.type:
            return {
                ...state,
                [whatToConnect]: false,
            };

        case WebSocketConnectAction.type:
        case WebSocketConnectingAction.type:
        case WebSocketDisconnectAction.type:
            return state;

        default:
            return state;
    }
};
