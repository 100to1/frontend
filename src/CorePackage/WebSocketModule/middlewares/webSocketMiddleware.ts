import { Dispatch, Middleware, MiddlewareAPI } from 'redux';

import {
    WebSocketConnectAction,
    WebSocketConnectedAction,
    WebSocketDisconnectAction,
    WebSocketDisconnectedAction,
} from '../actions/WebSocketConnectionActions';
import { WebSocketSendMessageAction } from '../actions/WebSocketSendMessageAction';
import { WebSocketMessage } from '../interfaces';

type TAction = WebSocketConnectAction | WebSocketDisconnectAction | WebSocketSendMessageAction;

let socket: WebSocket | null = null;

const onOpen = (dispatch: Dispatch) => () => {
    dispatch(new WebSocketConnectedAction());
};

const onClose = (dispatch: Dispatch) => () => {
    dispatch(new WebSocketDisconnectedAction());
};

const onMessage = (dispatch: Dispatch) => (event: MessageEvent) => {
    const message: WebSocketMessage = JSON.parse(event.data);

    dispatch(message.action);
};

export const webSocketMiddleware: Middleware = ({
    dispatch,
}: MiddlewareAPI) => next => (action: TAction) => {
    switch (action.type) {
        case WebSocketConnectAction.type:
            if (socket !== null) {
                socket.close();
            }

            // connect to the remote host
            socket = new WebSocket(action.host);

            // websocket handlers
            socket.onopen = onOpen(dispatch);
            socket.onclose = onClose(dispatch);
            socket.onmessage = onMessage(dispatch);

            break;

        case WebSocketDisconnectAction.type:
            if (socket !== null) {
                socket.close();
            }

            socket = null;

            break;

        case WebSocketSendMessageAction.type:
            socket?.send(JSON.stringify(action.message));

            break;

        default:
            return next(action);
    }
};
