export function endpoint(
    strings: TemplateStringsArray,
    ...keys: string[]
): string {
    const raw = strings.reduce((result, string, index) => {
        if (index < keys.length) {
            result += `${string}${keys[index]}`;
        } else {
            result += string;
        }

        return result;
    }, '');

    return `${process.env.BASE_PATH}${raw}`;
}
