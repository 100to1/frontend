import { MimeType } from '../enums';

export class ResponseHandler {
    public static commonHandler(response: Response): Promise<any> {
        if (!response.ok) {
            throw new Error(response.statusText);
        }

        if (response.redirected) {
            ResponseHandler.handleRedirect(response);
        }

        switch (response.status) {
            case 204:
                return ResponseHandler.handleNoContentResponse();
            default:
                return ResponseHandler.handleDefaultResponse(response);
        }
    }

    private static handleRedirect(response: Response): void {
        const targetLocation = response.url;

        if (targetLocation === null) {
            throw new Error('Location is empty');
        }

        location.replace(targetLocation);
    }

    private static handleNoContentResponse(): Promise<void> {
        return Promise.resolve();
    }

    private static handleDefaultResponse(response: Response): Promise<any> {
        const contentType = response.headers.get('Content-Type');

        if (contentType === null) {
            return Promise.resolve();
        } else if (contentType.includes(MimeType.APPLICATION_JSON)) {
            return ResponseHandler.handleJsonResponse(response);
        } else if (contentType.includes(MimeType.TEXT_HTML)) {
            return ResponseHandler.handleHtmlResponse(response);
        } else if (contentType.includes(MimeType.APPLICATION_EXCEL)) {
            return ResponseHandler.handleExcelResponse(response);
        } else {
            throw new Error('Unsupported content-type');
        }
    }

    private static handleJsonResponse(response: Response): Promise<any> {
        if (!response.ok) {
            return response.json().then(json => {
                return Promise.reject(json);
            });
        }

        return response.json();
    }

    private static handleExcelResponse(response: Response): Promise<Blob> {
        if (!response.ok) {
            return response.text().then(text => {
                return Promise.reject(text);
            });
        }

        return response.blob();
    }

    private static handleHtmlResponse(response: Response): Promise<string> {
        if (!response.ok) {
            return response.text().then(text => {
                return Promise.reject(text);
            });
        }

        return response.text();
    }

    private constructor() {}
}
