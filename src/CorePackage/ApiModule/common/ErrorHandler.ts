import { MimeType } from '../enums';

export class ErrorHandler {
    public static async commonHandler(response: Response): Promise<any> {
        if (response.ok) {
            return response;
        }

        return ErrorHandler.handleDefaultResponse(response);
    }

    private static async handleDefaultResponse(
        response: Response,
    ): Promise<any> {
        const contentType = response.headers.get('Content-Type');

        if (contentType === null) {
            return Promise.reject();
        } else if (contentType.includes(MimeType.APPLICATION_JSON)) {
            return await ErrorHandler.handleJsonResponse(response);
        } else if (contentType.includes(MimeType.TEXT_HTML)) {
            return await ErrorHandler.handleHtmlResponse(response);
        } else {
            throw new Error('Unsupported content-type');
        }
    }

    private static async handleJsonResponse(response: Response): Promise<void> {
        const json = await response.json();

        return Promise.reject(json);
    }

    private static async handleHtmlResponse(response: Response): Promise<void> {
        const text = await response.text();

        throw new Error(text);
    }

    private constructor() {}
}
