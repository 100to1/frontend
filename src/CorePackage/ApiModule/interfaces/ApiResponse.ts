import { ApiMeta } from '../types';

export interface ApiResponse<T> {
    data: T;
    meta?: ApiMeta;
}
