export interface Exception {
    error: {
        message: string;
        stack: string;
    };
}
