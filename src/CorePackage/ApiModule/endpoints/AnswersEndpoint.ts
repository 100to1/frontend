import { HttpMethod, MimeType, HttpCredentials } from '../enums';
import { ErrorHandler } from '../common/ErrorHandler';
import { ResponseHandler } from '../common/ResponseHandler';
import { ApiResponse, Exception } from '../interfaces';
import { endpoint } from '../common/endpoint';
import { AnswerDTO, Question } from '../../MainModule/interfaces';

export class AnswersEndpoint {
    public static getByQuestionId(
        questionId: Question['id'],
    ): Promise<ApiResponse<AnswerDTO[]>> {
        return fetch(endpoint`/answers?questionId=${String(questionId)}`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch((exception: Exception) =>
                console.error(exception.error.message),
            );
    }

    private constructor() {}
}
