import qs from 'qs';

import { HttpMethod, MimeType, HttpCredentials } from '../enums';
import { ErrorHandler } from '../common/ErrorHandler';
import { ResponseHandler } from '../common/ResponseHandler';
import { ApiResponse, Exception } from '../interfaces';
import { endpoint } from '../common/endpoint';
import { Pagination, Question } from '../../MainModule/interfaces';

export class QuestionsEndpoint {
    public static getQuestions(
        limit: Pagination['limit'],
        offset: Pagination['offset'],
    ): Promise<ApiResponse<Question[]>> {
        const serializedRequestParams = qs.stringify({
            'limit': limit,
            'offset': offset,
        });

        const urlSearchParams = new URLSearchParams(serializedRequestParams);

        return fetch(endpoint`/questions?${String(urlSearchParams)}`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch((exception: Exception) =>
                console.error(exception.error.message),
            );
    }

    public static getById(
        id: Question['id'],
    ): Promise<ApiResponse<Question>> {
        return fetch(endpoint`/questions/${String(id)}`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch((exception: Exception) =>
                console.error(exception.error.message),
            );
    }

    public static getRandom(): Promise<ApiResponse<Question>> {
        return fetch(endpoint`/questions/random`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch((exception: Exception) =>
                console.error(exception.error.message),
            );
    }

    private constructor() {}
}
