export type ApiMeta = {
    offset: number;
    limit: number;
    total: number;
};
