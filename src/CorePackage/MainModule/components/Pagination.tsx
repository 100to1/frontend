import * as React from 'react';
import autobind from 'autobind-decorator';

import * as css from './Pagination.pcss';
import { Button } from './Button';

type Props = {
    from: number;
    to: number;
    limit: number;
    offset: number;
    total: number;
    onPageChange: (offset: number) => void;
};

export class Pagination extends React.PureComponent<Props> {
    public static readonly defaultProps = {
        onPageChange: () => ({}),
    };

    public render() {
        const { from, to, limit, offset, total } = this.props;

        return (
            <div className={css.component}>
                <div className={css.info}>
                    <span>
                        {from}-{to} из {total} вопросов
                    </span>
                </div>
                <div className={css.buttons}>
                    <Button
                        className={css.button}
                        title={'Назад'}
                        disabled={offset < limit}
                        onClick={this.handlePrevBtnClick}
                    >
                        <span className={css.arrow} />
                    </Button>
                    <Button
                        className={css.button}
                        title={'Вперед'}
                        disabled={offset + limit >= total}
                        onClick={this.handleNextBtnClick}
                    >
                        <span className={css.arrow} />
                    </Button>
                </div>
            </div>
        );
    }

    @autobind
    private handlePrevBtnClick(
        event: React.MouseEvent<HTMLButtonElement>,
    ): void {
        event.preventDefault();

        const { limit, offset, onPageChange } = this.props;

        if (offset < limit) {
            return;
        }

        const newOffset = offset - limit;

        onPageChange(newOffset);
    }

    @autobind
    private handleNextBtnClick(
        event: React.MouseEvent<HTMLButtonElement>,
    ): void {
        event.preventDefault();

        const { limit, offset, total, onPageChange } = this.props;

        if (offset + limit >= total) {
            return;
        }

        const newOffset = offset + limit;

        onPageChange(newOffset);
    }
}
