import * as React from 'react';

import * as css from './MainLayout.pcss';
import { Preloader } from '../../MediaModule/components/Preloader';
import { SoundSwitcherContainer } from '../containers/SoundSwitcherContainer';

export type Props = {
    isFetching: boolean;
    isHostConnected: boolean;
    isPlayerConnected: boolean;
};

export const MainLayout: React.FunctionComponent<Props> = ({
    isFetching,
    isHostConnected,
    isPlayerConnected,
    children,
}) => (
    <main className={css.main}>
        {isHostConnected && isPlayerConnected && (
            <div className={css.soundSwitcher}>
                <SoundSwitcherContainer />
            </div>
        )}

        {children}

        {isFetching && (
            <div className={css.preloader}>
                <Preloader />
            </div>
        )}
    </main>
);
