import * as React from 'react';
import MaterialTextField, {
    Input,
    Props as TextFieldProps,
} from '@material/react-text-field';
import '@material/react-text-field/dist/text-field.css';

import * as css from './TextField.pcss';
import { classList } from '../common';

type Props = Omit<TextFieldProps<HTMLInputElement>, 'children'> &
    Input['props'];

export const TextField: React.FunctionComponent<Props> = ({
    className,
    label,
    dense = true,
    outlined = true,
    value,
    onChange,
}) => (
    <MaterialTextField
        label={label}
        dense={dense}
        outlined={outlined}
        className={classList({
            [css.textField]: true,
            [className as string]: className !== undefined,
        })}
        floatingLabelClassName={css.floatingLabel}
    >
        <Input value={value} className={css.input} onChange={onChange} />
    </MaterialTextField>
);
