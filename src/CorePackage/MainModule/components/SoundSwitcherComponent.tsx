import * as React from 'react';

import * as css from './SoundSwitcherComponent.pcss';
import { VolumeOn } from '../../MediaModule/components/VolumeOn';
import { VolumeOff } from '../../MediaModule/components/VolumeOff';

export type Props = {
    isSoundSwitchedOn: boolean;
    onSoundSwitch: () => void;
};

export const SoundSwitcherComponent: React.FunctionComponent<Props> = ({
    isSoundSwitchedOn,
    onSoundSwitch,
}) => (
    <div
        className={css.component}
        onClick={onSoundSwitch}
        title={isSoundSwitchedOn ? 'Выключить звук' : 'Включить звук'}
    >
        {isSoundSwitchedOn ? <VolumeOn /> : <VolumeOff />}
    </div>
);
