import * as React from 'react';
import MaterialButton, { ButtonProps } from '@material/react-button';
import '@material/react-button/dist/button.css';

import * as css from './Button.pcss';
import { classList } from '../common';
import { ButtonType } from '../enums';

type Props = ButtonProps<HTMLAnchorElement | HTMLButtonElement> & {
    buttonType?: ButtonType;
};

export const Button: React.FunctionComponent<Props> = ({
    buttonType,
    children,
    className,
    dense = false,
    disabled,
    raised = true,
    title,
    onClick,
}) => (
    <MaterialButton
        className={classList({
            [className as string]: className !== undefined,
            [css.button]: true,
            [css.buttonError]: buttonType === ButtonType.Error,
            [css.buttonSuccess]: buttonType === ButtonType.Success,
        })}
        dense={dense}
        disabled={disabled}
        raised={raised}
        title={title}
        onClick={onClick}
    >
        {children}
    </MaterialButton>
);
