import { createSelector } from 'reselect';
import { RootState } from '../interfaces';

export const getAppState = createSelector(
    (state: RootState) => state,
    state => state.App,
);
