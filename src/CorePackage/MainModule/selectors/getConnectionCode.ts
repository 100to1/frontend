import { createSelector } from 'reselect';
import { getAppState } from './getAppState';

export const getConnectionCode = createSelector(
    getAppState,
    app => app.connectionCode,
);
