import { createSelector } from 'reselect';
import { getAppState } from './getAppState';

export const getIsPlayerConnected = createSelector(
    getAppState,
    app => app.isPlayerConnected,
);
