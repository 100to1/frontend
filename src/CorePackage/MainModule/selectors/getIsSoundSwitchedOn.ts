import { createSelector } from 'reselect';

import { getAppState } from './getAppState';

export const getIsSoundSwitchedOn = createSelector(
    getAppState,
    app => app.isSoundSwitchedOn,
);
