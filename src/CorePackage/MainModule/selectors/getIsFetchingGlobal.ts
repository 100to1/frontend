import { createSelector } from 'reselect';
import { RootState } from '../interfaces';

export const getIsFetchingGlobal = createSelector(
    (state: RootState) => state,
    state => Object.values(state).some(value => value.isFetching === true),
);
