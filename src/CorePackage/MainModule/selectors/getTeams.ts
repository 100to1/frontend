import { createSelector } from 'reselect';
import { getAppState } from './getAppState';

export const getTeams = createSelector(getAppState, app => app.teams);
