import { createSelector } from 'reselect';
import { getAppState } from './getAppState';

export const getIsHostConnected = createSelector(
    getAppState,
    app => app.isHostConnected,
);
