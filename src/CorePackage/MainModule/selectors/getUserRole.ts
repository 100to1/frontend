import { createSelector } from 'reselect';
import { getAppState } from './getAppState';

export const getUserRole = createSelector(getAppState, app => app.userRole);
