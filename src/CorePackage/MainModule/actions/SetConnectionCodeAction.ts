import { Action } from 'redux';

export class SetConnectionCodeAction implements Action {
    public static readonly type = 'SET_CONNECTION_CODE';

    public readonly type = SetConnectionCodeAction.type;

    public constructor(readonly payload: string) {}
}
