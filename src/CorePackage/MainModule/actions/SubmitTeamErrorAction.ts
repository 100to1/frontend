import { Action } from 'redux';

import { Team } from '../interfaces';

export class SubmitTeamErrorAction implements Action {
    public static readonly type = 'SUBMIT_TEAM_ERROR';

    public readonly type = SubmitTeamErrorAction.type;
    public constructor(
        readonly payload: {
            teamId: Team['id'];
            errors: number;
        },
    ) {}
}
