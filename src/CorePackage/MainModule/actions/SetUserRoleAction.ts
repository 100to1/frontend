import { Action } from 'redux';
import { UserRole } from '../enums';

export class SetUserRoleAction implements Action {
    public static readonly type = 'SET_CLIENT_ROLE';

    public readonly type = SetUserRoleAction.type;

    public constructor(readonly payload: UserRole) {}
}
