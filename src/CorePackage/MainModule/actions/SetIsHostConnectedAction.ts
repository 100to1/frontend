import { Action } from 'redux';

export class SetIsHostConnectedAction implements Action {
    public static readonly type = 'SET_IS_HOST_CONNECTED';

    public readonly type = SetIsHostConnectedAction.type;

    public constructor(readonly payload: boolean) {}
}
