import { Action } from 'redux';

export class SetIsPlayerConnectedAction implements Action {
    public static readonly type = 'SET_IS_PLAYER_CONNECTED';

    public readonly type = SetIsPlayerConnectedAction.type;

    public constructor(readonly payload: boolean) {}
}
