import { Action } from 'redux';
import { Team } from '../interfaces';

export class SetTeamAction implements Action {
    public static readonly type = 'SET_TEAM';

    public readonly type = SetTeamAction.type;

    public constructor(readonly payload: Team) {}
}

export class ChangeTeamPointsAction implements Action {
    public static readonly type = 'CHANGE_TEAM_POINTS';

    public readonly type = ChangeTeamPointsAction.type;

    public constructor(readonly payload: Pick<Team, 'id' | 'points'>) {}
}
