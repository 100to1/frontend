import { Action } from 'redux';

export class SwitchSoundAction implements Action {
    public static readonly type = 'SWITCH_SOUND';

    public readonly type = SwitchSoundAction.type;

    public constructor() {}
}
