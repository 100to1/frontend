import { Action } from 'redux';
import { Team } from '../interfaces';

export class AddTeamAction implements Action {
    public static readonly type = 'ADD_TEAM';

    public readonly type = AddTeamAction.type;

    public constructor(readonly payload: Team) {}
}
