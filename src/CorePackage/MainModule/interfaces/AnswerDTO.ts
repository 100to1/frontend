import { Question } from './Question';

export interface AnswerDTO {
    id: string;
    name: string;
    id_question: Question['id'];
    priority: string;
}
