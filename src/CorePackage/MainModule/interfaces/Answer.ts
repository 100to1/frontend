import { Question } from './Question';

export interface Answer {
    id: string;
    name: string;
    questionId: Question['id'];
    priority: number;
    isGuessed: boolean;
    votes?: number;
}
