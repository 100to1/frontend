export interface Team {
    id: string;
    name: string;
    points: number;
    errors: number;
}
