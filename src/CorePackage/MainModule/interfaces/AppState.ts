import { UserRole } from '../enums';
import { Team } from './Team';

export interface AppState {
    connectionCode?: string;
    isHostConnected: boolean;
    isPlayerConnected: boolean;
    isSoundSwitchedOn: boolean;
    teams: Team[];
    userRole: UserRole;
}
