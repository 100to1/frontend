import { AppState } from './AppState';
import { State as QuestionsListState } from '../../../QuestionsListPackage/MainModule/interfaces';
import { State as RoundState } from '../../../RoundPackage/MainModule/interfaces';

export interface RootState {
    App: AppState;
    QuestionsList: QuestionsListState;
    Round: RoundState;
}
