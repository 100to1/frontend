import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';

import {
    Props as SoundSwitcherComponentProps,
    SoundSwitcherComponent,
} from '../components/SoundSwitcherComponent';
import { RootState } from '../interfaces';
import { getIsSoundSwitchedOn } from '../selectors';
import { SwitchSoundAction } from '../actions/SwitchSoundAction';
import { WebSocketSendMessageAction } from '../../WebSocketModule/actions/WebSocketSendMessageAction';

type Props = SoundSwitcherComponentProps;
type StateProps = Pick<Props, 'isSoundSwitchedOn'>;
type DispatchProps = Pick<Props, 'onSoundSwitch'>;
type OwnProps = Pick<Props, never>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    isSoundSwitchedOn: getIsSoundSwitchedOn(state),
});

const mapDispatchProps: MapDispatchToProps<
    DispatchProps,
    OwnProps
> = dispatch => ({
    onSoundSwitch: () => {
        dispatch(new SwitchSoundAction());
        dispatch(
            new WebSocketSendMessageAction({
                action: {
                    type: SwitchSoundAction.type,
                },
            }),
        );
    },
});

export const SoundSwitcherContainer = connect(
    mapStateToProps,
    mapDispatchProps,
)(SoundSwitcherComponent);
