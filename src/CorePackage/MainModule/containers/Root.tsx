import * as React from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import isMobile from 'ismobilejs';

import { RootRouter } from './RootRouter';
import { RootState } from '../interfaces';
import {
    getIsFetchingGlobal,
    getIsHostConnected,
    getIsPlayerConnected,
    getUserRole,
} from '../selectors';
import { SetUserRoleAction } from '../actions/SetUserRoleAction';
import { UserRole } from '../enums';

type Props = RootRouter['props'] & {
    onMount: () => void;
};

class Root extends React.PureComponent<Props> {
    public componentDidMount(): void {
        this.props.onMount();
    }

    public render() {
        return (
            <React.Fragment>
                <RootRouter {...this.props} />
            </React.Fragment>
        );
    }
}

type OwnProps = {};
type StateProps = Pick<
    Props,
    'isFetching' | 'isHostConnected' | 'isPlayerConnected' | 'userRole'
>;
type DispatchProps = Pick<Props, 'onMount'>;

const mapStateToProps: MapStateToProps<
    StateProps,
    OwnProps,
    RootState
> = state => ({
    isFetching: getIsFetchingGlobal(state),
    isHostConnected: getIsHostConnected(state),
    isPlayerConnected: getIsPlayerConnected(state),
    userRole: getUserRole(state),
});

const mapDispatchToProps: MapDispatchToProps<
    DispatchProps,
    OwnProps
> = dispatch => ({
    onMount: () => {
        const userRole = isMobile().phone ? UserRole.Host : UserRole.Player;

        dispatch(new SetUserRoleAction(userRole));
    },
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
    pure: false,
})(Root);
