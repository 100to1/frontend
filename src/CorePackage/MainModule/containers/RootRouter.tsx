import * as React from 'react';
import { Redirect, Switch } from 'react-router-dom';

import * as Index from '../../../IndexPackage';
import * as TeamNames from '../../../TeamNamesPackage';
import * as QuestionsList from '../../../QuestionsListPackage';
import * as Round from '../../../RoundPackage';
import { MainLayout, Props as MainLayoutProps } from '../components/MainLayout';
import { UserRole } from '../enums';

type Props = MainLayoutProps & {
    userRole: UserRole;
};

export class RootRouter extends React.Component<Props> {
    public render() {
        const {
            isHostConnected,
            isPlayerConnected,
            userRole,
        } = this.props;

        return (
            <MainLayout {...this.props}>
                <Switch>
                    <Index.Router userRole={userRole} />
                    {(!isPlayerConnected || !isHostConnected) && (
                        <Redirect to={Index.IndexRoute.url()} />
                    )}
                    <TeamNames.Router userRole={userRole} />
                    <QuestionsList.Router />
                    <Round.Router userRole={userRole} />
                </Switch>
            </MainLayout>
        );
    }
}
