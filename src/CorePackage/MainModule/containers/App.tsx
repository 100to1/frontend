import * as React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

import { configureStore, history } from '../common';
import Root from './Root';

type Props = {};
type State = {};

export class App extends React.PureComponent<Props, State> {
    public readonly store = configureStore();

    public render() {
        return (
            <Provider store={this.store}>
                <Router history={history}>
                    <Root />
                </Router>
            </Provider>
        );
    }
}
