import { Reducer } from 'redux';

import {
    ChangeTeamPointsAction,
    SetTeamAction,
} from '../actions/SetTeamAction';
import { AppState } from '../interfaces';
import { defaultState } from '../common';

type TAction = SetTeamAction | ChangeTeamPointsAction;

export const teamReducer: Reducer<AppState, TAction> = (
    state = defaultState,
    action,
) => {
    switch (action.type) {
        case SetTeamAction.type:
            return {
                ...state,
                teams: [...state.teams, action.payload],
            };

        case ChangeTeamPointsAction.type:
            return {
                ...state,
                teams: state.teams.map(team => {
                    if (action.payload.id === team.id) {
                        return {
                            ...team,
                            points: team.points + action.payload.points,
                        };
                    }

                    return team;
                }),
            };

        default:
            return state;
    }
};
