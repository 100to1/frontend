import { Reducer } from 'redux';

import { AppState } from '../interfaces';
import { defaultState } from '../common';
import { SetUserRoleAction } from '../actions/SetUserRoleAction';

type TAction = SetUserRoleAction;

export const setUserRoleReducer: Reducer<AppState, TAction> = (
    state = defaultState,
    action,
): AppState => {
    switch (action.type) {
        case SetUserRoleAction.type:
            return {
                ...state,
                userRole: action.payload,
            };

        default:
            return state;
    }
};
