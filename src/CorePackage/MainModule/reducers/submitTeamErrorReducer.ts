import { Reducer } from 'redux';

import { SubmitTeamErrorAction } from '../actions/SubmitTeamErrorAction';
import { AppState } from '../interfaces';
import { defaultState } from '../common';

type TAction = SubmitTeamErrorAction;

export const submitTeamErrorReducer: Reducer<AppState, TAction> = (
    state = defaultState,
    action,
): AppState => {
    switch (action.type) {
        case SubmitTeamErrorAction.type:
            return {
                ...state,
                teams: state.teams.map(team => {
                    if (team.id === action.payload.teamId) {
                        return {
                            ...team,
                            errors: action.payload.errors,
                        };
                    }

                    return team;
                }),
            };

        default:
            return state;
    }
};
