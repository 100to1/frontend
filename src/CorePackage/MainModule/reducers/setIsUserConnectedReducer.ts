import { Reducer } from 'redux';

import { AppState } from '../interfaces';
import { defaultState } from '../common';
import { SetIsPlayerConnectedAction } from '../actions/SetIsPlayerConnectedAction';
import { SetIsHostConnectedAction } from '../actions/SetIsHostConnectedAction';

type TAction = SetIsPlayerConnectedAction | SetIsHostConnectedAction;

export const setIsUserConnectedReducer: Reducer<AppState, TAction> = (
    state = defaultState,
    action,
): AppState => {
    switch (action.type) {
        case SetIsPlayerConnectedAction.type:
            return {
                ...state,
                isPlayerConnected: action.payload,
            };

        case SetIsHostConnectedAction.type:
            return {
                ...state,
                isHostConnected: action.payload,
            };

        default:
            return state;
    }
};
