import { Reducer } from 'redux';

import { AddTeamAction } from '../actions/AddTeamAction';
import { AppState } from '../interfaces';
import { defaultState } from '../common';

type TAction = AddTeamAction;

export const addTeamReducer: Reducer<AppState, TAction> = (
    state = defaultState,
    action,
): AppState => {
    switch (action.type) {
        case AddTeamAction.type:
            return {
                ...state,
                teams: [...state.teams, action.payload],
            };

        default:
            return state;
    }
};
