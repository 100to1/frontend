import reduceReducers from 'reduce-reducers';

import { AppState } from '../interfaces';
import { defaultState } from '../common';
import { addTeamReducer } from './addTeamReducer';
import { setUserRoleReducer } from './setUserRoleReducer';
import { setConnectionCodeReducer } from './setConnectionCodeReducer';
import { teamReducer } from './teamReducer';
import { submitTeamErrorReducer } from './submitTeamErrorReducer';
import { webSocketConnectionReducer } from '../../WebSocketModule/reducers/webSocketConnectionReducer';
import { setIsUserConnectedReducer } from './setIsUserConnectedReducer';
import { switchSoundReducer } from './switchSoundReducer';

export const appStateReducer = reduceReducers<AppState, any>(
    defaultState ?? null,
    addTeamReducer,
    setConnectionCodeReducer,
    setIsUserConnectedReducer,
    setUserRoleReducer,
    submitTeamErrorReducer,
    teamReducer,
    switchSoundReducer,
    webSocketConnectionReducer,
);
