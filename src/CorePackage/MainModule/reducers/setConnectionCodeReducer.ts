import { Reducer } from 'redux';

import { AppState } from '../interfaces';
import { defaultState } from '../common';
import { SetConnectionCodeAction } from '../actions/SetConnectionCodeAction';

type TAction = SetConnectionCodeAction;

export const setConnectionCodeReducer: Reducer<AppState, TAction> = (
    state = defaultState,
    action,
): AppState => {
    switch (action.type) {
        case SetConnectionCodeAction.type:
            return {
                ...state,
                connectionCode: action.payload,
            };

        default:
            return state;
    }
};
