import { Reducer } from 'redux';

import { SwitchSoundAction } from '../actions/SwitchSoundAction';
import { AppState } from '../interfaces';
import { defaultState } from '../common';

type TAction = SwitchSoundAction;

export const switchSoundReducer: Reducer<AppState, TAction> = (
    state = defaultState,
    action,
): AppState => {
    switch (action.type) {
        case SwitchSoundAction.type:
            return {
                ...state,
                isSoundSwitchedOn: !state.isSoundSwitchedOn,
            };

        default:
            return state;
    }
};
