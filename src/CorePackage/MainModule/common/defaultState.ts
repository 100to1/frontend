import { UserRole } from '../enums';
import { AppState } from '../interfaces';

export const defaultState: AppState = {
    userRole: UserRole.Player,
    isHostConnected: false,
    isPlayerConnected: false,
    isSoundSwitchedOn: false,
    teams: [],
};
