import {
    applyMiddleware,
    combineReducers,
    compose,
    createStore,
    Reducer,
} from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

import { classActionMiddleware } from '../middlewares/classActionMiddleware';
import { webSocketMiddleware } from '../../WebSocketModule/middlewares/webSocketMiddleware';
import { RootState } from '../interfaces';
import { appStateReducer } from '../reducers';
import { questionsListReducer } from '../../../QuestionsListPackage/MainModule/reducers';
import { roundReducer } from '../../../RoundPackage/MainModule/reducers';

export function configureStore() {
    const reducer: Reducer<RootState> = combineReducers({
        App: appStateReducer,
        QuestionsList: questionsListReducer,
        Round: roundReducer,
    });
    const extendedCompose = (window as any)
        .__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    const composeEnhancers =
        typeof extendedCompose === 'undefined' ? compose : extendedCompose;

    return createStore(
        reducer,
        composeEnhancers(
            applyMiddleware(
                classActionMiddleware,
                thunk,
                promiseMiddleware,
                webSocketMiddleware,
            ),
        ),
    );
}
