export function makeFirstUpperCase(text: string): string {
    return text
        .split('. ')
        .map(el => `${el[0].toUpperCase()}${el.substring(1)}`)
        .join('. ');
}
