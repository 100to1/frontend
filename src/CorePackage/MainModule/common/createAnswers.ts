import { Answer, AnswerDTO } from '../interfaces';

export function createAnswers(answerDTO: AnswerDTO): Answer {
    return {
        id: answerDTO.id,
        name: answerDTO.name,
        questionId: answerDTO.id_question,
        priority: Number(answerDTO.priority),
        isGuessed: false,
    };
}
