class Config {
    constructor() {
        this.distDirectory = 'dist';
        this.apiGateways = {
            local: `http://localhost:3000`,
            heroku: `https://secret-hollows-00900.herokuapp.com`,
        };
    }

    getApiUrl(isProduction) {
        return isProduction ? this.apiGateways.heroku : this.apiGateways.local;
    }
}

module.exports = new Config();
